import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import numpy as np
import os

mycolor = 'royalblue'

plotpath = '{}plots'.format(__file__.strip('projection_wireframe.py'))
if not os.path.exists(plotpath):
    os.makedirs(plotpath)  
    
# initialize figure
fig = plt.figure()
figsize = 15
fig.set_size_inches(figsize,figsize)
ax = fig.add_subplot(111, projection='3d')

# =============================================================================
# set up part of a globe
# =============================================================================
r = 100
az_steps = np.radians(np.linspace(-180,180,num=19))
el_steps = np.radians(np.linspace(0,90,num=10))

az_range = np.radians(np.linspace(-180,180,num=200))
el_range = np.radians(np.linspace(0,90,num=100))

kwargs_thin = dict(color='k', lw=0.5, alpha=0.3)
kwargs_mid = dict(color='k', lw=1, alpha=0.5)
kwargs_bold = dict(color='k', lw=1.5)

# plot parallels
for el in el_steps[:-1]:
    x = r*np.cos(el)*np.cos(az_range)
    y = r*np.cos(el)*np.sin(az_range)
    z = r*np.sin(el)
    ax.plot(x, y, z, **kwargs_thin)
    
# plot meridians
for az in az_steps[:-1]:
    x = r*np.cos(el_range)*np.cos(az)
    y = r*np.cos(el_range)*np.sin(az)
    z = r*np.sin(el_range)
    ax.plot(x, y, z, **kwargs_thin) 

# =============================================================================
# plot detector with rays connecting to the planet 
# =============================================================================
# focal point location
origin = np.array([500,0,120])
# where should the boresight vector point toward
target = np.array([0,0,74.3])
# angle by which the boresight is pointing downwards
alpha = -np.arcsin((origin[2]-target[2])/origin[0])

# start with boresight pointing towards -x
boresight = np.array([-1,0,0])
yw = 5
zw = 4.5
corners = np.array([[-100, yw, zw],
                    [-100, -yw, zw],
                    [-100, -yw, -zw],
                    [-100, yw, -zw],
                    ], dtype=np.float64)
for iii in range(np.shape(corners)[0]):
    corners[iii,:] /= np.linalg.norm(corners[iii])
edges = {}
edgenames = 'abcd'
for ectr in range(len(edgenames)):
    start = corners[ectr]
    end = corners[(ectr+1)%4]
    diff = end-start
    edges[edgenames[ectr]] = np.array([(start+diff*iii/99)/np.linalg.norm(start+diff*iii/100)
                                        for iii in range(100)])

# rotate the detector a bit
beta = np.radians(0)
c = np.cos(beta)
s = np.sin(beta)
m = np.array([[1,0,0],
              [0,c,s],
              [0,-s,c]])
boresight = np.matmul(m, boresight)
corners = np.array([np.matmul(m, corners[iii,:]) for iii in range(np.shape(corners)[0])])
for ectr in range(len(edgenames)):
    tmp = edges[edgenames[ectr]]
    edges[edgenames[ectr]] = np.array([np.matmul(m, tmp[iii,:]) for iii in range(np.shape(tmp)[0])])

# rotate everything downwards
c = np.cos(alpha)
s = np.sin(alpha)
m = np.array([[c,0,s],
              [0,1,0],
              [-s,0,c]])
boresight = np.matmul(m, boresight)
corners = np.array([np.matmul(m, corners[iii,:]) for iii in range(np.shape(corners)[0])])
for ectr in range(len(edgenames)):
    tmp = edges[edgenames[ectr]]
    edges[edgenames[ectr]] = np.array([np.matmul(m, tmp[iii,:]) for iii in range(np.shape(tmp)[0])])
    
# get length of a vectors to intersect with the planet
def findPlanetIntLen(vec, origin):
    ctr = 0
    length = 1
    d = np.linalg.norm(origin+length*vec)-100
    while d > 0.1:
        length += d
        d = np.linalg.norm(origin+length*vec)-100
        ctr += 1
        if ctr > 100:
            return 0
    return length

# get length between origin and flat plane
def findPlaneLen(vec,origin):
    d = 300
    c = np.dot(vec, boresight)/np.linalg.norm(vec)/np.linalg.norm(boresight)
    return d/c

def xyz2razel(xyz):
    x,y,z = xyz
    r = np.sqrt(np.sum(xyz**2))
    el = np.arcsin(z/r)
    az =np.arctan2(y, x)
    return np.array([r, az, el])

corners_len = np.array([findPlanetIntLen(corners[iii,:], origin) for iii in range(np.shape(corners)[0])])
corners_len_plane = np.array([findPlaneLen(corners[iii,:], origin) for iii in range(np.shape(corners)[0])])
edges_len = {}
edges_len_plane = {}
for ectr in range(len(edgenames)):
    tmp = edges[edgenames[ectr]]
    edges_len[edgenames[ectr]] = np.array([findPlanetIntLen(tmp[iii,:], origin) for iii in range(np.shape(tmp)[0])])
    edges_len_plane[edgenames[ectr]] = np.array([findPlaneLen(tmp[iii,:], origin) for iii in range(np.shape(tmp)[0])])
    
# plot corner lines
for cctr in range(np.shape(corners)[0]):
    tmp = np.array([origin+corners_len_plane[cctr]*corners[cctr,:],
                    origin+corners_len[cctr]*corners[cctr,:]])
    ax.plot(*tmp.T, color=mycolor, lw=0.5)

# plot surface pixel edges
edges_loc_xyz = {}
edges_loc_razel = {}
for ectr in range(len(edgenames)):
    tmp_edge = edges[edgenames[ectr]]
    tmp_len = edges_len[edgenames[ectr]]
    edges_loc_xyz[edgenames[ectr]] = np.array([
            origin+tmp_len[iii]*tmp_edge[iii,:] for iii in range(np.shape(tmp_edge)[0])])
    edges_loc_razel[edgenames[ectr]] = np.array([
            xyz2razel(edges_loc_xyz[edgenames[ectr]][iii,:]) for iii in range(np.shape(tmp_edge)[0])])
    ax.plot(*edges_loc_xyz[edgenames[ectr]].T, color=mycolor, lw=2)
edges_loc_xyz_flat = edges_loc_xyz['a']
edges_loc_razel_flat = edges_loc_razel['a']
for ectr in range(1,len(edgenames)):
    edges_loc_razel_flat = np.append(edges_loc_razel_flat, edges_loc_razel[edgenames[ectr]], axis=0)
    edges_loc_xyz_flat = np.append(edges_loc_xyz_flat, edges_loc_xyz[edgenames[ectr]], axis=0)
    
# plot plane pixel edges
edges_plane_xyz = {}
for ectr in range(len(edgenames)):
    tmp_edge = edges[edgenames[ectr]]
    tmp_len = edges_len_plane[edgenames[ectr]]
    tmp = np.array([
            origin+tmp_len[iii]*tmp_edge[iii,:] for iii in range(np.shape(tmp_edge)[0])])
    ax.plot(*tmp.T, color=mycolor, lw=2)
    edges_plane_xyz[edgenames[ectr]] = tmp
    if not ectr:
        edges_plane_xyz_flat = tmp
    else:
        edges_plane_xyz_flat = np.append(edges_plane_xyz_flat, tmp, axis=0)
    
# plot covered surface
x = edges_loc_xyz_flat[:,0]
y = edges_loc_xyz_flat[:,1]
z = edges_loc_xyz_flat[:,2]
verts = [list(zip(x, y, z))]
coll = Poly3DCollection(verts, alpha=0.2)
coll.set_facecolor('k')
ax.add_collection3d(coll, zs='z')

# plot plane surface
x = edges_plane_xyz_flat[:,0]
y = edges_plane_xyz_flat[:,1]
z = edges_plane_xyz_flat[:,2]
verts = [list(zip(x, y, z))]
coll = Poly3DCollection(verts, alpha=0.2)
coll.set_facecolor('k')
ax.add_collection3d(coll, zs='z')

# plot ray surfaces
for ectr in range(len(edgenames)):
    if not ectr%2:
        continue
    tmp = np.append(edges_plane_xyz[edgenames[ectr]], edges_loc_xyz[edgenames[ectr]][::-1], axis=0)
    x = tmp[:,0]
    y = tmp[:,1]
    z = tmp[:,2]
    verts = [list(zip(x, y, z))]
    coll = Poly3DCollection(verts, alpha=0.1)
    coll.set_facecolor(mycolor)
    ax.add_collection3d(coll, zs='z')
    
# get max/min lon/colat
corners_sph = np.array([xyz2razel(origin+corners_len[iii]*corners[iii,:]) for iii in range(np.shape(corners)[0])])
min_el = np.min(corners_sph[:,2])
max_el = np.max(corners_sph[:,2])
min_az = np.min(corners_sph[:,1])
max_az = np.max(corners_sph[:,1])

# plot parallels
tmp = el_steps[(el_steps>min_el)&(el_steps<max_el)]
for el in tmp:
    edges_selec = edges_loc_razel_flat[np.where(np.abs(edges_loc_razel_flat[:,2]-el) < np.radians(0.2)), 1]
    tmp_az_min = np.min(edges_selec)-np.radians(5)
    tmp_az_max = np.max(edges_selec)+np.radians(5)
    az_range_plot = az_range[(az_range>tmp_az_min)&(az_range<tmp_az_max)]
    x = r*np.cos(el)*np.cos(az_range_plot)
    y = r*np.cos(el)*np.sin(az_range_plot)
    z = np.repeat(r*np.sin(el), len(az_range_plot))
    ax.plot(x, y, z, **kwargs_bold)
    vecs = np.array([x,y,z]).T - origin
    vecs = np.array([vecs[iii,:]/np.linalg.norm(vecs[iii,:]) for iii in range(np.shape(vecs)[0])])
    lengths = np.array([findPlaneLen(vecs[iii,:], origin) for iii in range(np.shape(vecs)[0])])
    tmp = np.array([origin+lengths[iii]*vecs[iii,:] for iii in range(np.shape(vecs)[0])])
    ax.plot(*tmp.T, **kwargs_mid)
    
# plot meridians
tmp = az_steps[(az_steps>min_az)&(az_steps<max_az)]
for az in tmp:
    edges_selec = edges_loc_razel_flat[np.where(np.abs(edges_loc_razel_flat[:,1]-az) < np.radians(1)), 2]
    tmp_el_min = np.min(edges_selec)-np.radians(5)
    tmp_el_max = np.max(edges_selec)+np.radians(5)
    el_range_plot = el_range[(el_range>tmp_el_min)&(el_range<tmp_el_max)]
    x = r*np.cos(el_range_plot)*np.cos(az)
    y = r*np.cos(el_range_plot)*np.sin(az)
    z = r*np.sin(el_range_plot)
    ax.plot(x, y, z, **kwargs_bold) 
    vecs = np.array([x,y,z]).T - origin
    vecs = np.array([vecs[iii,:]/np.linalg.norm(vecs[iii,:]) for iii in range(np.shape(vecs)[0])])
    lengths = np.array([findPlaneLen(vecs[iii,:], origin) for iii in range(np.shape(vecs)[0])])
    tmp = np.array([origin+lengths[iii]*vecs[iii,:] for iii in range(np.shape(vecs)[0])])
    ax.plot(*tmp.T, **kwargs_mid)


# =============================================================================
# set axes equal
# =============================================================================

def set_axes_radius(ax, origin, radius):
    ax.set_xlim3d([origin[0] - radius, origin[0] + radius])
    ax.set_ylim3d([origin[1] - radius, origin[1] + radius])
    ax.set_zlim3d([origin[2] - radius, origin[2] + radius])

def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    limits = np.array([
        ax.get_xlim3d(),
        ax.get_ylim3d(),
        ax.get_zlim3d(),
    ])

    origin = np.mean(limits, axis=1)
    radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
    set_axes_radius(ax, origin, radius)
    
ax.set_aspect('equal')
set_axes_equal(ax)
plt.axis('off')

fig.subplots_adjust(left=0, right=1, bottom=0, top=1)
margin = 4.6
bbox = fig.bbox_inches.from_bounds(margin+0.6,
                                   1.18*margin+0.2,
                                   fig.get_figwidth()-2*margin,
                                   fig.get_figheight()-2*1.18*margin)

ax.view_init(elev=25, azim=30)
plt.draw()
plt.savefig('{}/projection_wireframe_01.pdf'.format(plotpath), bbox_inches=bbox)
plt.savefig('{}/projection_wireframe_01.png'.format(plotpath), bbox_inches=bbox, dpi=300)

plt.close()

