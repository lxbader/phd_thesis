import cubehelix
from pathlib import Path
import re
from wordcloud import WordCloud, STOPWORDS

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5,
                         minLight=0.0, maxLight=0.75, gamma=1.5)

#%%

textpath = Path('./raw_text_noref')
for ctr, file in enumerate(textpath.glob('*.tex')):
    with open(file, 'r') as f:
        text = f.read()
    # remove math mode
    text = re.sub(r'(\$[^\$]+\$)',
                  ' ', text)
    # remove citations
    text = re.sub(r'\\cite\S+',
                  ' ', text)
    # remove refs
    text = re.sub(r'\\ref\S+',
                  ' ', text)
    # remove all remaining LaTeX (catches most things)
    text = re.sub(r'\\.*?}',
                  ' ', text)
    
    # Create stopword list:
    stopwords = set(STOPWORDS)
    stopwords.update(['Figure', 'Fig'])
    
    # Generate a word cloud image
    wordcloud = WordCloud(width=1600,
                          height=600,
                          colormap=cmap_UV,
                          min_font_size=24,
                          relative_scaling=0.5,
                          prefer_horizontal=0.7,
                          collocations=False,
                          stopwords=stopwords,
                          background_color='white').generate(text)
    
    wordcloud.to_file(f'wordcloud_{ctr+1}.png')
    
