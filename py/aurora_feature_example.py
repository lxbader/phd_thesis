# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
import get_image
import glob
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import remove_solar_reflection

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

rsr = remove_solar_reflection.RemoveSolarReflection()

#%%
savepath = '{}/phd_thesis/py/plots'.format(gitpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)
    
def makeplot(ax, samplefile, imagedata=None, cax=None, cbarh=False,
             tmin=0, tmax=360, rmin=0, rmax=30,
             KR_MIN=0.5, KR_MAX=30, setallticks=False):

    if imagedata is None:
        data, _, _ = get_image.getRedUVIS(samplefile)
    else:
        data = imagedata
    data[data<0.1] = 0.1
    
    # plot
    lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
    colatbins = np.linspace(0, 30, num=np.shape(data)[1]+1)
    quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
    quad.set_clim(0, KR_MAX)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    ax.set_facecolor('0.25')
    
    # plot colorbar
    if not (cax is None):
        if cbarh:
            cbar = plt.colorbar(quad, cax=cax, extend='both', orientation='horizontal')
            cbar.set_label('Intensity (kR)', labelpad=10, fontsize=14)
        else:
            cbar = plt.colorbar(quad, cax=cax, extend='both')
            cbar.set_label('Intensity (kR)', labelpad=10 if KR_MAX<50 else 14, rotation=270, fontsize=14)
        cbar.set_ticks(np.append(np.append(np.arange(KR_MIN,1,0.1),
                                           np.arange(1,10,1)),
                                 np.arange(10,KR_MAX+1,10)))
        cbar.ax.tick_params(labelsize=12)
    
    ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
    ticklabels = ['00','06','12','18']
    for iii in range(len(ticklabels)):
        tmp = np.degrees(ticks[iii])
        if tmp > tmax:
            continue
        else:
            if tmp < tmin%360 and not tmp==tmax%360:
                continue
        if (tmin==0 and tmax==360) or setallticks:
            txt = ax.text(ticks[iii], rmax-3 if not setallticks else rmax-4, ticklabels[iii], color='w', fontsize=12 if setallticks else 14, fontweight='bold',
                          ha='center',va='center', zorder=5)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])

    ax.set_xticks(ticks)
    if tmin>=0:
        ax.set_xticks(np.linspace(0, 2*np.pi, 25), minor=True)
    else:
        ax.set_xticks(np.linspace(-np.pi, np.pi, 25), minor=True)
    ax.set_xticklabels([])
    
    ax.set_yticks([10,20,30])
    ax.set_yticks([5,15,25], minor=True)
    ax.set_yticklabels([])
    ax.grid('on', color='0.8', linewidth=1, which='major')
    ax.grid('on', color='0.8', linewidth=0.5, linestyle='--', which='minor')

    ax.set_rorigin(0)
    ax.set_rmax(rmax)
    ax.set_rmin(rmin)
    ax.set_theta_zero_location("N")
    ax.set_thetamin(tmin)
    ax.set_thetamax(tmax)
    
#%%
# =============================================================================
# Plot
# =============================================================================

files = np.array([
        '2008_129T08_53_38',
        '2017_232T06_50_53',
        '2017_252T02_46_23',
        
        '2008_254T19_10_10',
        '2017_167T16_25_36',
        '2008_254T17_49_10',
        
        '2008_239T02_15_51',
        '2008_109T12_52_41',
        '2017_167T19_04_16',
        ])
searchpath = '{}/UVIS/PDS_UVIS/PROJECTIONS_SELEC_RES10'.format(datapath)
#searchpath = '{}/UVIS/PDS_UVIS/PROJECTIONS_RES2_ISS'.format(datapath)

tlims = np.array([[0, 90],
                  [-90, 90],
                  [270, 360],
                  [0, 180],
                  [0, 360],
                  [180, 360],
                  [90, 180],
                  [90, 270],
                  [180, 270],
                  ])
    
arrowlocs = np.array([[2.2, 26, 18],
                      [2.2, 6, 14.5],
                      [20.8, 29, 23],
                      
                      [5.5, 3, 12],
                      [0, 0, 0],
                      [15.3, 24, 15.1],
                      
                      [8.3, 17, 24.5],
                      [11.1, 24, 13],
                      [14.8, 27, 17],
                      ])
            
fig = plt.figure()
mult = 12
fig.set_size_inches(mult*0.975, mult*1.06)
#fig.set_figheight(mult)
gs = gridspec.GridSpec(8, 6, wspace=0, hspace=0,
                       width_ratios=(1,0.2,1,1,0.2,1),
                       height_ratios=(1,0.2,1,1,0.2,1, 0.25, 0.1))

for panel in range(9):
    thisfile = glob.glob('{}/**/{}.fits'.format(searchpath, files[panel]), recursive=True)[0]
    
    rsr.calculate(thisfile)
    image = np.copy(rsr.redImage_aur)

    rowmin = 0 if panel<3 else (2 if panel<6 else 5)
    colmin = 0 if panel%3==0 else (2 if panel%3==1 else 5)
    rowmax = rowmin + (2 if panel == 4 else 1)
    colmax = colmin + (2 if panel == 4 else 1)
    
    vert = [3,5]
    hori = [1,7]
    inv = [1,5]
    noninv = [3,7]
    
    for iii in (range(2) if (panel in vert or panel in hori) else range(1)):
        if iii==1 and panel in hori:
            colmin += 1
            colmax += 1
        elif iii==1 and panel in vert:
            rowmin += 1
            rowmax += 1
        ax = plt.subplot(gs[rowmin:rowmax,
                            colmin:colmax],
                        projection='polar')
        tmin = tlims[panel,0] #if not panel in inv else tlims[panel,0]+90
        tmax = tlims[panel,1] #if not panel in noninv else tlims[panel,0]-90
        if panel in inv:
            if iii == 0:
                tmin += 90
                ax.spines['start'].set_color('0.8')
            else:
                tmax -= 90
                ax.spines['end'].set_color('0.8')
        if panel in noninv:
            if iii == 0:
                tmax -= 90
                ax.spines['end'].set_color('0.8')
            else:
                tmin += 90
                ax.spines['start'].set_color('0.8')
                
            
        makeplot(ax, thisfile, imagedata = image,
                 tmin=tmin, tmax=tmax,
                 KR_MIN=0.1, KR_MAX=30,
                 cax = plt.subplot(gs[-1,:]) if iii==0 else None,
                 cbarh=True)
        
        loct, start, stop = arrowlocs[panel, :]
        arrowstyle = '->,head_length=9, head_width=3.75'
        arr = patches.FancyArrowPatch((loct/12*np.pi, start),
                                           (loct/12*np.pi, stop),
                                           arrowstyle=arrowstyle,
                                           color='crimson',
                                           linewidth=2.5,
                                           zorder=3)
        arr.set_path_effects([PathEffects.withStroke(linewidth=4.2, foreground='w')])
        ax.add_patch(arr)
        
        def add_time(text):
            ax.text(1 if panel in hori else 0.5, -0.04, text,
                  transform=ax.transAxes,
                  ha='center', va='top',
                  color='k', fontsize=11)
        tstamp = thisfile.split('/')[-1].split('\\')[-1].split('.')[0]
        tstamp = '{} {} ({})'.format(tstamp[:8].replace('_','-'),
                                    tstamp[9:].replace('_',':'),
                                    rsr.hem[0])
        
        if not iii:
            txt = ax.text(0.03 if panel%3<2 else 0.97, 0.97, '({})'.format('abcdefghijklmnop'[panel]),
                      transform=ax.transAxes, ha='left' if panel%3<2 else 'right',
                      va='top', color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
            if panel not in vert:
                add_time(tstamp)
        else:
            if panel in vert:
                add_time(tstamp)
            
plt.savefig('{}/aurora_feature_example_200.png'.format(savepath),
            bbox_inches='tight', dpi=200)
plt.savefig('{}/aurora_feature_example_120.png'.format(savepath),
            bbox_inches='tight', dpi=120)
plt.savefig('{}/aurora_feature_example_300.png'.format(savepath),
            bbox_inches='tight', dpi=300)
plt.show()
plt.close()

sys.exit()

#%% 
# =============================================================================
# Flashes 
# =============================================================================
files = np.array([
        '2008_195T08_27_35',
        '2008_195T08_42_31',
        '2008_195T08_57_19',
        '2008_195T09_12_23',
        '2008_195T09_27_19',
        '2008_195T09_46_23',
        '2008_195T09_57_11',
        '2008_195T10_12_07',
        '2008_195T10_27_03',
        '2008_195T10_41_59',
        ])
            
arrowlocs = np.array([[0, 0, 0],
                      [0, 0, 0],
                      [17.5, 21, 10.5],
                      [0, 0, 0],
                      [0, 0, 0],
                      
                      [0, 0, 0],
                      [0, 0, 0],
                      [19.2, 22, 11.5],
                      [0, 0, 0],
                      [0, 0, 0],
                      ])
            
searchpath = '{}/UVIS/PDS_UVIS/PROJECTIONS_SELEC_FLASHES_RES10'.format(datapath)
#searchpath = '{}/UVIS/PDS_UVIS/PROJECTIONS_RES2_ISS'.format(datapath)

fig = plt.figure()
mult = 4
fig.set_size_inches(mult*2.95, mult*2)
gs = gridspec.GridSpec(5,7, width_ratios=(1,)*5+(0.08,0.12), height_ratios=(1,1,0.3,1,1),
                       wspace=0., hspace=0.)

for panel in range(0,len(files)):
    row = 0 if not panel // 5 else 3
    col = panel % 5
    
    for rrr, tstart in zip([row, row+1], [270,180]):
        ax = plt.subplot(gs[rrr,col], projection='polar')
        
        thisfile = glob.glob('{}/**/{}.fits'.format(searchpath, files[panel]), recursive=True)[0]
        
        rsr.calculate(thisfile)
        image = np.copy(rsr.redImage_aur)
        
        if not panel and not rrr:
            cax = plt.subplot(gs[:,-1])
        else:
            cax = None
        makeplot(ax, thisfile, imagedata = image,
                 cax=cax,
                 tmin=tstart,
                 tmax=tstart+89.99 if rrr==row else tstart+90,
                 KR_MIN=0.5, KR_MAX=20,
                 setallticks=True)
        
        def add_time(text):
            ax.text(0.5, 1.11, text,
                  transform=ax.transAxes,
                  ha='center', va='center',
                  color='k', fontsize=11)
        tstamp = thisfile.split('/')[-1].split('\\')[-1].split('.')[0]
        tstamp = '{}'.format(tstamp[9:].replace('_',':'))
        
        if rrr==row:
            add_time(tstamp)
            ax.spines['start'].set_color('0.8')
            txt = ax.text(-0.05, 1.12, '({})'.format('abcdefghijklmnop'[panel]),
                      transform=ax.transAxes, ha='left', va='center',
                      color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        else:
            
            ax.spines['end'].set_color('0.8')
            
        loct, start, stop = arrowlocs[panel, :]
        arrowstyle = '->,head_length=9, head_width=3.75'
        arr = patches.FancyArrowPatch((loct/12*np.pi, start),
                                           (loct/12*np.pi, stop),
                                           arrowstyle=arrowstyle,
                                           color='crimson',
                                           linewidth=2.5,
                                           zorder=3)
        arr.set_path_effects([PathEffects.withStroke(linewidth=4.2, foreground='w')])
        ax.add_patch(arr)
            
plt.savefig('{}/aurora_flash_example.png'.format(savepath), bbox_inches='tight', dpi=200)
plt.close()


#%%  
# =============================================================================
# Dawn storm
# =============================================================================
files = np.array([
        '2017_257T03_41_35',
        '2017_257T04_32_47',
        '2017_257T05_23_53',
        '2017_257T06_17_13',
        
        '2017_257T07_09_34',
        '2017_257T08_04_46',
        '2017_257T08_59_55',
        '2017_257T09_58_19',
        
        '2017_257T10_56_15',
        '2017_257T11_58_55',
        '2017_257T14_02_52',
        '2017_257T15_01_32',
        ])
            
arrowlocs = np.array([[0, 0, 0],
                      [2.5, 28, 17],
                      [0, 0, 0],
                      [0, 0, 0],
                      
                      [0, 0, 0],
                      [0, 0, 0],
                      [0, 0, 0],
                      [0, 0, 0],
                      
                      [0, 0, 0],
                      [0, 0, 0],
                      [0, 0, 0],
                      [0, 0, 0],
                      ])
                        
searchpath = '{}/UVIS/PDS_UVIS/PROJECTIONS_SELEC_DAWNSTORM_RES10'.format(datapath)
#searchpath = '{}/UVIS/PDS_UVIS/PROJECTIONS_RES2_ISS'.format(datapath)

fig = plt.figure()
mult = 4
fig.set_size_inches(mult*3, mult*2.3)
gs = gridspec.GridSpec(3, 5, width_ratios=(1,)*4+(0.12,),
                       wspace=0.1, hspace=0.05)

for panel in range(0,len(files)):
    row = panel // 4
    col = panel % 4
    
    ax = plt.subplot(gs[row,col], projection='polar')
    
    thisfile = glob.glob('{}/**/{}.fits'.format(searchpath, files[panel]), recursive=True)[0]
    
    rsr.calculate(thisfile)
    image = np.copy(rsr.redImage_aur)
    
    if not panel:
        cax = plt.subplot(gs[:,-1])
    else:
        cax = None
    makeplot(ax, thisfile, imagedata = image,
             cax=cax,
             KR_MIN=0.5, KR_MAX=100,
             setallticks=True)
    
    def add_time(text):
        ax.text(0.5, 1.06, text,
              transform=ax.transAxes,
              ha='center', va='center',
              color='k', fontsize=11)
    tstamp = thisfile.split('/')[-1].split('\\')[-1].split('.')[0]
    tstamp = '{}'.format(tstamp[9:].replace('_',':'))
    
    add_time(tstamp)
    txt = ax.text(0, 1.06, '({})'.format('abcdefghijklmnop'[panel]),
              transform=ax.transAxes, ha='left', va='center',
              color='k', fontweight='bold', fontsize=15)
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
    
    loct, start, stop = arrowlocs[panel, :]
    arrowstyle = '->,head_length=9, head_width=3.75'
    arr = patches.FancyArrowPatch((loct/12*np.pi, start),
                                       (loct/12*np.pi, stop),
                                       arrowstyle=arrowstyle,
                                       color='crimson',
                                       linewidth=2.5,
                                       zorder=3)
    arr.set_path_effects([PathEffects.withStroke(linewidth=4.2, foreground='w')])
    ax.add_patch(arr)
                    
plt.savefig('{}/aurora_dawnstorm_example.png'.format(savepath), bbox_inches='tight', dpi=200)
plt.close()
