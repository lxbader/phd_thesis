import matplotlib.gridspec as gridspec
import matplotlib.patches as ptch
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.interpolate as sint

myblue = 'royalblue'
myred = 'crimson'

plotpath = '{}plots'.format(__file__.strip('ppo_model.py'))
if not os.path.exists(plotpath):
    os.makedirs(plotpath)  
    
# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]
    
phirange = np.arange(0,360,2)
#phirange = [30]

for ctr in range(len(phirange)):
    phi_ppo = phirange[ctr]

    # initialize figure
    fig = plt.figure()
    figsize = 8
    if len(phirange)>1:
        fig.set_size_inches(20,8.5)
    else:
        fig.set_size_inches(16,6.8)
#    fig.set_figwidth(2.5*figsize)
    gs = gridspec.GridSpec(1,2, wspace=0.1, width_ratios=(5,3))
    ax = plt.subplot(gs[0,0])
    ax2 = plt.subplot(gs[0,1])
    
    ax.text(-0.05, 1, '(a)', transform=ax.transAxes, fontweight='bold', fontsize=25,
            va='top', ha='left')
    ax2.text(0, 1, '(b)', transform=ax2.transAxes, fontweight='bold', fontsize=25,
            va='top', ha='left')
    
    # plot magnetopause
    MAX = 40
    xmp, rmp = mploc(0.05)
    val = np.where((np.abs(xmp)<MAX) & (np.abs(rmp)<MAX))[0]
    ax.plot(rmp[val], xmp[val], c='dimgrey', ls='-.')
    ax.plot(-rmp[val], xmp[val], c='dimgrey', ls='-.')
    
    # plot planet
    ax.add_patch(
            ptch.Wedge((0,0),1,-180,0,
                       facecolor='k', edgecolor='k',
                       zorder=10))
    ax.add_patch(
            ptch.Wedge((0,0),1,0,180,
                       facecolor='w', edgecolor='k',
                       zorder=10))
    
    # plot axes
    hl = 2
    YMAX = MAX-5
    XMAX = 0.6*MAX
    ax.arrow(0, -XMAX, 0, 2*XMAX-hl, head_width=2*hl/3, head_length=hl,
             fc='dimgrey', ec='dimgrey')
    ax.text(0, XMAX, 'X$_\mathrm{KSM}$', ha='center', va='top', fontsize=15,
            color='dimgrey')
    ax.arrow(-YMAX, 0, 2*YMAX-hl, 0, head_width=2*hl/3, head_length=hl,
             fc='dimgrey', ec='dimgrey')
    ax.text(YMAX, 0, 'Y$_\mathrm{KSM}$', ha='left', va='center', fontsize=15,
            color='dimgrey')
    
    # plot basic polar projection grid
    radii = np.arange(10,40,10)
    for rrr in radii:
        phi = np.linspace(0, 2*np.pi, num=361)
        ax2.plot(rrr*np.cos(phi), rrr*np.sin(phi),
                 c='dimgrey', ls='--')
    phis = np.arange(0,2*np.pi,np.pi/2)
    lts = ['12','18','00','06']
    for ppp, lll in zip(phis, lts):
        r = np.linspace(0,30, num=10)
        ax2.plot(r*np.sin(ppp), r*np.cos(ppp),
                 c='dimgrey', ls='--')
        ax2.text(26*np.sin(ppp), 26*np.cos(ppp), lll,
                 fontweight='bold', fontsize=22,
                 ha='center', va='center')
    
    def addPPON(ax, ax2, phi_ppo):
        # add circle at 15 RS
        phi = np.linspace(0, 2*np.pi, num=361)
        RS = 13.5
        ax.plot(RS*np.cos(phi), RS*np.sin(phi), c='dimgrey', ls='--')
        
        # add PPO arrow
        ax.add_patch(
                ptch.FancyArrowPatch(
                        (-17*np.sin(np.radians(phi_ppo)),
                         -17*np.cos(np.radians(phi_ppo))),
                        (17*np.sin(np.radians(phi_ppo)),
                         17*np.cos(np.radians(phi_ppo))),
                         arrowstyle='Fancy,head_length=20, head_width=10, tail_width=5',
                         color='k',
                         zorder=4))
                        
        # add PPO arrow
        ax2.add_patch(
                ptch.FancyArrowPatch(
                        (-21*np.sin(np.radians(phi_ppo)),
                         -21*np.cos(np.radians(phi_ppo))),
                        (21*np.sin(np.radians(phi_ppo)),
                         21*np.cos(np.radians(phi_ppo))),
                         arrowstyle='Fancy,head_length=20, head_width=10, tail_width=5',
                         color='k',
                         zorder=4))
                        
        ax.plot([-17*np.sin(np.radians(phi_ppo+90)), 17*np.sin(np.radians(phi_ppo+90))],
                [-17*np.cos(np.radians(phi_ppo+90)), 17*np.cos(np.radians(phi_ppo+90))],
                c='k',
                ls='-')
        ax2.plot([-21*np.sin(np.radians(phi_ppo+90)), 21*np.sin(np.radians(phi_ppo+90))],
                [-21*np.cos(np.radians(phi_ppo+90)), 21*np.cos(np.radians(phi_ppo+90))],
                c='k',
                ls='-')
        
        # add angle marker
        tmp = np.radians(np.arange(0, phi_ppo))
        ax.plot(10*np.sin(tmp), 10*np.cos(tmp), c='k', ls='-')
        ax.text(12*np.sin(np.radians(phi_ppo/2)),
                12*np.cos(np.radians(phi_ppo/2)),
                '$\Phi_\mathrm{N}$',
                ha='center',
                va='center',
                fontsize=15,
                fontweight='bold',
                color='k')
        
        psi_angles = np.radians(np.arange(0,-360,-90)+phi_ppo)
        psi_names = np.array(['$\Psi_\mathrm{{N}}={}^\circ$'.format(iii) for iii in range(0,360,90)])
        for ppp in range(len(psi_angles)):
            ax.text(22*np.sin(psi_angles[ppp]),
                    22*np.cos(psi_angles[ppp]),
                    psi_names[ppp],
                    ha='center',
                    va='center',
                    fontsize=13,
                    fontweight='bold',
                    color='k')
            ax2.text(26*np.sin(psi_angles[ppp]),
                    26*np.cos(psi_angles[ppp]),
                    psi_names[ppp],
                    ha='center',
                    va='center',
                    fontsize=13,
                    fontweight='bold',
                    color='k')
            
        # add upward / downward currents out in the equatorial plane
        numcirc = 5
        radii = np.linspace(RS/13.5,2*RS/13.5,num=int((numcirc+1)/2))
        radii = np.append(radii, radii[:-1][::-1])
        
        shift = np.sort(np.unique(radii))[::-1][:-1]**3
        shift = np.concatenate([shift, [0], -shift[::-1]])
        ang_offsets = np.linspace(-45,45,num=numcirc)
        ang_offsets += shift
         
        locs = np.array([phi_ppo+90, phi_ppo+270])
        dirs = ['down','up']
        for lll, ddd in zip(locs, dirs):
            angles = np.radians(lll+ang_offsets)
            
            dist = 13.5
            xs = dist*np.cos(angles)
            ys = dist*np.sin(angles)
            
            dist2 = 15
            xs2 = dist2*np.cos(angles)
            ys2 = dist2*np.sin(angles)
            for iii in range(len(xs)):
                ax.add_patch(ptch.Circle([ys[iii], xs[iii]], radii[iii],
                                           color=myred, fill=False,
                                           linewidth=3, zorder=10))
                ax2.add_patch(ptch.Circle([ys2[iii], xs2[iii]], radii[iii]+0.2,
                                           color=myred, fill=False,
                                           linewidth=3, zorder=10))
                
                if ddd=='up':
                    ax.text(ys[iii], xs[iii], 'x', color=myred,
                            ha='center', va='center',
                            fontweight='bold', fontsize=radii[iii]**1.5*10)
                    ax2.add_patch(ptch.Circle([ys2[iii],xs2[iii]], radii[iii]**2/4,
                                               color=myred, zorder=10))
                else:
                    ax.add_patch(ptch.Circle([ys[iii],xs[iii]], radii[iii]**2/4,
                                               color=myred, zorder=10))
                    ax2.text(ys2[iii], xs2[iii], 'x', color=myred,
                            ha='center', va='center',
                            fontweight='bold', fontsize=radii[iii]**1.5*8)
        # add atmospheric flows
        angles = np.array([90,270])+phi_ppo
        ccws = [False, True]
        for aaa, ccw in zip(angles, ccws):
            if not ccw:
                z = np.linspace(0,2*np.pi, num=500)
            else:
                z = np.linspace(2*np.pi,0, num=500)
            c_r = 15
            w_r = 10
            r = np.cos(z)*w_r/2+c_r
            
            c_t = np.radians(aaa)
            w_t = 5/6*np.pi
            t = np.sin(z)*w_t/2+c_t
            
            valid = np.where((z<np.radians(95)) | (z>np.radians(265)))[0]
            x = r*np.sin(t)
            y = r*np.cos(t)
            
            tmp = np.argmax(np.diff(valid))
            n = len(x)-len(valid)
            xs = np.linspace(x[valid[tmp]], x[valid[tmp+2]], num=n)
            ys = np.linspace(y[valid[tmp]], y[valid[tmp+2]], num=n)
            
            invalid = np.where(~((z<np.radians(95)) | (z>np.radians(265))))[0]
            x[invalid] = xs
            y[invalid] = ys
            
            ax2.plot(x, y, c=myblue, lw=4)
            diff = 5
            cent = np.array([30,300,470])+5
            arrowstyle = '->, head_length=20, head_width=10'
            for iii in range(3):
                frac = 1 if iii!=1 else 1
                
                ax2.add_patch(ptch.FancyArrowPatch(
                        (x[int(cent[iii]-diff)]*frac,
                           y[int(cent[iii]-diff)]*frac),
                        (x[int(cent[iii]+diff)]*frac,
                           y[int(cent[iii]+diff)]*frac),
                           arrowstyle=arrowstyle,
                           color=myblue,
                           linewidth=4))
                        
            # add perturbation field
            xs = np.linspace(-40, 40, num=36)
            ys = np.linspace(-25, 25, num=36)
            X, Y = np.meshgrid(xs, ys)
            
            m = np.array([np.sin(np.radians(phi_ppo)), np.cos(np.radians(phi_ppo))])
            
            vx = np.full_like(X, np.nan)
            vy = np.full_like(Y, np.nan)
            
            for iii in range(np.shape(vx)[0]):
                for jjj in range(np.shape(vx)[1]):
                    if np.linalg.norm(r) == 0:
                        vec = 2/3*m
                    else:
                        r = np.array([X[iii,jjj], Y[iii,jjj]])
                        r_hat = r/np.linalg.norm(r)
                        vec = ((3*np.dot(m,r_hat)*r_hat-m)/(4*np.pi*np.linalg.norm(r)**3)
                        + m/np.linalg.norm(r)**4)
                    vx[iii,jjj] = vec[0]
                    vy[iii,jjj] = vec[1]
            
            f = sint.interp1d(xmp, rmp, fill_value=np.nan, bounds_error=False)
            RMP = f(Y)
            tmp = np.where(~(np.abs(X)<RMP))
            vx[tmp] = np.nan
            vy[tmp] = np.nan
            
            a = np.array([np.cos(np.radians(phi_ppo)), -np.sin(np.radians(phi_ppo))])
            b = np.linspace(-12, 12, 21)
            xystart = np.outer(a,b)
            
            ax.streamplot(xs, ys, vx, vy, density=5,
                          linewidth=0.5, color='0.6',
                          arrowsize=0.001,
                          start_points=xystart.T)
            
#            # use num=36
#            l = np.sqrt(vx**2+vy**2)
#            ax.quiver(X, Y,
#                      vx/l, -vy/l,
#                      #np.log10(l), cmap='inferno',
#                      color='k',
#                      units='x', width=0.08, alpha=0.3,
#                      headwidth=7, headlength=12)

                        
    addPPON(ax, ax2, phi_ppo)
    
    ax.axis('equal')
    ax.set_ylim([25,-30])
    ax.set_xlim([-MAX,MAX])
    ax.axis('off')
    
    ax2.set_xlim([-30,30])
    ax2.axis('equal')
    ax2.set_ylim(-np.array([*ax2.get_ylim()]))
    ax2.axis('off')
    
    if len(phirange)==1:
        plt.savefig('{}/ppo_model.pdf'.format(plotpath), bbox_inches='tight')
        plt.savefig('{}/ppo_model.png'.format(plotpath), bbox_inches='tight', dpi=300)
    else:
        savepath = r'E:\Box Sync\Plots\ppo_model'
        if not os.path.exists(savepath):
            os.makedirs(savepath)
        plt.savefig('{}/ppo_model_{:03d}.png'.format(savepath, ctr), bbox_inches='tight', dpi=100)
    plt.show()
    plt.close()
    
if len(phirange)>1:
    import glob
    import imageio
    
    frames = glob.glob('{}/*.png'.format(savepath))
    frames = np.sort(frames)
    frames = np.array([imageio.imread(iii) for iii in frames])
    imageio.mimsave('{}/ppo_model.gif'.format(savepath), frames, format='GIF', duration=0.05)

