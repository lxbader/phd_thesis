\section{Space Plasmas}

\subsection{The Plasma State}

Plasma is one of the four fundamental states of matter, the others being solid, liquid and gas. In the plasma state, atoms are at least partly dissociated into their positive and negative components, i.e., ions and electrons. As charge is conserved, plasma is electrically ``quasi-neutral'' on the whole. 

The interactions between single charged particles within an ideal plasma are determined by electromagnetic forces acting between them. The spatial scale over which one ion can influence its surroundings, limited by collective shielding of its potential by nearby electrons, is given by the Debye length,
\begin{equation}
	\lambda_D = \sqrt{\frac{\varepsilon_0 k_B T_e}{n_e e^2}}.
\end{equation}
Hereby $\varepsilon_0$ is the permittivity of free space and $k_B$ is Boltzmann's constant, while $T_e$, $n_e$ and $e$ are the electron temperature, density and charge, respectively. Shielding of a plasma ion can only occur if the number of electrons within the Debye sphere, a sphere of radius $\lambda_D$ around the considered ion, is sufficiently large. This is given when
\begin{equation}
	N_D = \frac{4\pi}{3} n_e \lambda_D^3 \gg 1,
\end{equation}
which is essentially the case for plasma at high temperatures and low densities and certainly true for solar wind and magnetospheric plasmas considered in this thesis.

Plasma does not need to be fully ionized in order to behave like an ideal plasma, but may also contain neutral particles. It is only required that the collision frequency between charged and neutral constituents is lower than the plasma frequency,
\begin{equation}
	\omega_{pe} = \sqrt{\frac{n_e e^2}{m_e \varepsilon_0}},
\end{equation}
which describes the natural oscillation frequency of electrons of mass $m_e$ about the ions in a quasi-neutral plasma.

As plasma predominantly consists of charged particles and is assumed collisionless due to the typically low number density, its behaviour is governed by electric and magnetic fields, $\mathbf E$ and $\mathbf B$. These are described by Maxwell's equations,
\begin{align}
	\nabla\cdot \mathbf E &= \frac{\rho_q}{\varepsilon_0} \label{eq::gaussE}\\
	\nabla\cdot \mathbf B &= 0 \label{eq::gaussB}\\
	\nabla\times \mathbf E &= -\frac{\partial\mathbf B}{\partial t} \label{eq::faraday}\\
	\nabla\times \mathbf B &= \mu_0 \left(\mathbf j + \varepsilon_0\frac{\partial\mathbf E}{\partial t}\right), \label{eq::ampere}
\end{align}
with $\rho_q$ as the charge density, $\mu_0$ as the permeability of free space and $\mathbf j$ as the current density. Hereby equations~\eqref{eq::gaussE} and \eqref{eq::gaussB} are Poisson's law and Gauss' law for magnetic fields, \eqref{eq::faraday} is the Maxwell-Faraday equation and \eqref{eq::ampere} is the Amp\`{e}re-Maxwell law.

\subsection{Single Particle Motion}
In this section the motion of individual charged particles in the presence of electric and magnetic fields will be described, unaffected by interactions with surrounding particles like in a real plasma. A single particle with charge $q$ and velocity $\mathbf v$ in an electromagnetic field will experience the Lorentz force,
\begin{equation}
	\mathbf F = m\frac{d\mathbf v}{dt} = q\left(\mathbf E + \mathbf v\times \mathbf B\right).
\end{equation}
In presence of a uniform magnetic field and absence of an electric field, the resulting motion reduces to a simple gyration about the magnetic field lines whose direction depends on the sign of the particle's charge. The angular frequency of this motion is known as gyrofrequency,
\begin{equation}
	\omega_g = \frac{|q|B}{m},
\end{equation}
with $B$ as the magnetic field strength, while the center of the gyration is known as the guiding center. The kinetic energy of the particle remains constant during the gyration, as the Lorentz force acts perpendicular to the particle's direction of motion,
\begin{equation}
	\frac{d}{dt}\left(\frac{1}{2}m\mathbf v^2\right) = m\mathbf v\frac{d\mathbf v}{dt} = q\mathbf v\cdot \left(\mathbf v \times \mathbf B\right) = 0.
\end{equation}
The velocity vector $\mathbf v$ is hereby always oriented perpendicular to the magnetic field, such that $\mathbf v = \mathbf{v_\perp}$. 

\begin{wrapfigure}{R}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/introduction/prolss_gyration_eq_2}
	\caption[Illustration of charged particle gyration]{Illustration of a charged particle's helical motion in a homogeneous magnetic field. Modified from \citet{prolss_physics_2004}.\\
	}
	\label{fig::intro::gyration}
	\vspace{\baselineskip}
\end{wrapfigure}

If the particle has an additional velocity component $\mathbf{v_\parallel}$ parallel to the magnetic field, the total velocity is given by $\mathbf v = \mathbf{v_\perp} + \mathbf{v_\parallel}$ and the particle gyrates around the field lines while moving along them, describing a helical path such as shown in Figure~\ref{fig::intro::gyration}. The angle at which it moves with respect to the magnetic field,
\begin{equation}
	\alpha = \arctan\left(\frac{\mathbf{v_\perp}}{\mathbf{v_\parallel}}\right),
\end{equation}
is called pitch angle. Values of $\alpha=0^\circ$ and $\alpha=180^\circ$ then describe particles moving parallel and antiparallel, respectively, to the background magnetic field (i.e., field-aligned), while $\alpha=90^\circ$ corresponds to pure gyration.

%\begin{wrapfigure}{L}{0.5\textwidth}
%	\centering
%	\includegraphics[width=\linewidth]{./img/introduction/russell_drift_1}
%	\caption[$\mathbf E\times \mathbf B$ drift illustration]{Illustration of $\mathbf E\times \mathbf B$ drift for positive ions and electrons. Taken from \citet{russell_space_2016}.}
%	\label{fig::intro::drift}
%\end{wrapfigure}

The motion of individual charged particles becomes more complicated in the presence of electric fields or other nonelectromagnetic forces. Any additional force $\mathbf F$ will result in a drift motion of the particle's guiding center, perpendicular to both $\mathbf B$ and $\mathbf F$ itself. The drift velocity $\mathbf{v_D}$ of the charged particle is given by
\begin{equation}
	\mathbf{v_D} = \frac{\mathbf F \times \mathbf B}{qB^2}.
\end{equation}
For an electric field $\mathbf E$ perpendicular to $\mathbf B$, the additional force experienced by a charged particle is $\mathbf{F_E} = q\mathbf E$ such that
\begin{equation}
	\mathbf{v_D} = \frac{\mathbf E \times \mathbf B}{B^2}.
\end{equation}
We find that the drift velocity is in this case independent of both charge and mass of the particle, such that electrons and ions will drift in the same direction with a common velocity and no currents are induced; this is known as the ``$\mathbf E \times \mathbf B$ drift''.

Other forces typically acting on charged particles in planetary magnetospheres are related to the gradient and curvature of the magnetic field and to gravity and polarization, i.e.,
\begin{align}
	\mathbf{F_\nabla} &= -\mu \nabla B\\
	\mathbf{F_C} &= mv_\parallel^2\frac{\mathbf{R_C}}{R_C^2}\\
	\mathbf{F_G} &= -m\mathbf g\\
	\mathbf{F_P} &= -m\frac{d\mathbf E}{dt},	
\end{align}
with $\mathbf{R_C}$ as the local radius of magnetic field curvature and $\mathbf g$ as the gravitational acceleration. The gradient force is dependent on the magnetic moment,
\begin{equation}
	\mu = \frac{mv_\perp^2}{2B} = \frac{W_\perp}{B},
\end{equation}
with $W_\perp$ as the kinetic energy associated with the perpendicular gyration. The magnetic moment is a characteristic constant also known as the first adiabatic invariant, as it is conserved if changes in the magnetic field are small compared to the time scale of the gyromotion.

As a charged particle moves slowly from a low into a high magnetic field strength region, its perpendicular velocity will increase to conserve its magnetic moment. With the particle's total energy $W = W_\parallel + W_\perp$ being unchanged in the absence of parallel electric fields, this can only occur through a reduction of its parallel velocity. The motion along the magnetic field line hence slows down as the magnetic field becomes stronger, up to the point where $\mathbf{v_\parallel}=0$ and all kinetic energy is associated with the perpendicular gyration with a pitch angle of $\alpha=90^\circ$. As the mirror force $\mathbf{F_\nabla}$ continues acting on the particle regardless of its parallel velocity $\mathbf{v_\parallel}$, it is reflected back into regions of lower magnetic field, or ``mirrored''. 

\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/introduction/prolss_bounce_mod_eq_2}
	\caption[Schematic of particle bounce in a planetary magnetic field]{Schematic of a charged particle's bounce motion in a planetary magnetic field, confined by magnetic mirrors at either end. Modified from \citet{prolss_physics_2004}.}
	\label{fig::intro::bounce}
\end{wrapfigure}

Pitch angle and magnetic field strength along the particle's trajectory are hereby related through
\begin{equation}
	\frac{B}{\sin^2(\alpha)} = \text{const.},
\end{equation}
such that the field strength at the mirror point ($\sin^2(\alpha) = 1$) can easily be determined with knowledge of a particle's pitch angle at a location of known magnetic field strength. For a dipole field such as found in planetary magnetospheres, there are two mirror points on each field line, essentially forming a magnetic bottle and trapping charged particles which bounce between them such as shown in Figure~\ref{fig::intro::bounce}. However, depending on the initial/equatorial pitch angle a particle may not reach a region of large enough field strength to mirror before entering the atmosphere where it loses its energy in collisions with atmospheric neutrals and causes aurorae. At a point along the magnetic field line where the magnetic field strength is $B$, all particles with a pitch angle smaller than the loss cone angle
\begin{equation}
	\alpha_{LC} = \arcsin\left(\sqrt{\frac{B}{B_{max}}}\right)
\end{equation}
are lost to the atmosphere. Here $B_{max}$ denotes the magnetic field strength just above the atmosphere.

\subsection{Kinetic Theory and Phase Space}
A macroscopic plasma, essentially a collection of particles, is difficult to describe in terms of the single particle behaviour detailed above. It is much easier to characterize the state of a plasma in terms of phase space density, i.e. by counting the number of particles located at a certain point in configuration space $\mathbf r = \Bigl(\begin{smallmatrix}x\\y\\z\end{smallmatrix}\Bigr)$ which have a velocity $\mathbf v = \Bigl(\begin{smallmatrix}v_x\\v_y\\v_z\end{smallmatrix}\Bigr)$. The phase space density for a collection of particles $i$ located at $\mathbf r_i$ with velocity $\mathbf v_i$, respectively, is given by
\begin{equation}
	f(\mathbf r, \mathbf v, t) = \sum_i \delta\big( \mathbf r-\mathbf r_i(t)\big) \delta\big( \mathbf v-\mathbf v_i(t)\big),
\end{equation}
with $\delta( \mathbf r-\mathbf r_i) = \delta(x-x_i)\delta(y-y_i)\delta(z-z_i)$ and $\delta( \mathbf v-\mathbf v_i)$, similarly, as the three-dimensional Dirac delta functions.

This description allows the derivation of ensemble properties by calculating the moments of the distribution. Hereby the first moment gives the number density,
\begin{equation}
	n(\mathbf r, t) = \int f(\mathbf r, \mathbf v, t) d\mathbf v,
\end{equation}
and the second moment the bulk velocity,
\begin{equation}
	u(\mathbf r, t) = \frac{1}{n(\mathbf r, t)} \int \mathbf v f(\mathbf r, \mathbf v, t) d\mathbf v,
\end{equation}
while the third moment relates to the kinetic energy,
\begin{equation}
	\langle \frac{1}{2}m\mathbf v^2\rangle = \frac{1}{n(\mathbf r, t)} \int \frac{1}{2} m\mathbf v^2 f(\mathbf r, \mathbf v, t) d\mathbf v.
\end{equation}
Phase space distributions cannot be directly measured, but can be inferred from the differential particle flux $J(W, \Omega, \mathbf r, t)$ within an energy band $dW$ about $W$ and within a solid angle $d\Omega$ observed at position $\mathbf r$ and at time $t$; this is the quantity typically measured by spacecraft-borne particle detectors. It is clear that the number density of particles depends on the phase space density as 
\begin{equation}
	dn = f(\mathbf r, \mathbf v, t) d\mathbf v,
\end{equation}
or
\begin{equation}
	dn = f(\mathbf r, \mathbf v, t) v^2dv~d\varphi~\sin\vartheta d\vartheta = f(\mathbf r, \mathbf v, t) v^2 dv d\Omega 
\end{equation}
in spherical coordinates. Multiplying this quantity by $v$ provides the differential flux, such that 
\begin{equation}
	J(W, \Omega, \mathbf r, t)dWd\Omega = f(\mathbf r, \mathbf v, t) v^3 dv d\Omega.
\end{equation}
Finally, simplifying this expression using $dW = mvdv$ yields
\begin{equation}
	f(\mathbf r, \mathbf v, t) = \frac{m^2}{2W} J(W, \Omega, \mathbf r, t),
\end{equation}
a straightforward relation between differential particle flux and phase space distribution function.

\subsection{Magnetohydrodynamics}
In the theoretical concept of \acp{MHD}, the collective behaviour of charged particles in a plasma is described by treating plasma as a conducting fluid. This framework is applicable if the gyroperiod of the plasma constituents is smaller than the characteristic time scale of changes in the fluid and the scale size of the plasma significantly exceeds the charged particles' gyroradius. Similar to unmagnetised fluids, mass and momentum are conserved and viscous and pressure forces act on the particles; furthermore, charge conservation applies and the Lorentz force acts within a conducting plasma.

In \ac{MHD}, the macroscopic plasma properties are described by the mass density, $\rho_m$, the bulk velocity, $\mathbf u$, and the pressure, $P$. We assume a plasma which is quasineutral within the characteristic length and time scales, such that $n_e \approx n_i = n$. The continuity equation, describing the conservation of mass, is then
\begin{equation}
	\frac{\partial \rho_m}{\partial t} + \nabla\cdot(\rho_m\mathbf u) = 0
\end{equation}
while the momentum conservation equation is given by
\begin{equation} \label{eq::momentum}
	\rho_m\left(\frac{\partial\mathbf u}{\partial t} + (\mathbf u \cdot \nabla)\mathbf u\right) = -\nabla P + \mathbf j \times \mathbf B + \rho_m\mathbf g.
\end{equation}
The first two terms on the right hand-side of equation~\eqref{eq::momentum} describe the effects of a thermal pressure gradient and the magnetic forces, respectively. We can find another expression for the second term by taking the cross product of Amp\`{e}re's law~\eqref{eq::ampere} with $\mathbf B$, giving
\begin{equation}
	\mathbf j \times \mathbf B = -\nabla\left(\frac{B^2}{2\mu_0}\right) + \frac{1}{\mu_0}(\mathbf B \cdot \nabla)\mathbf B.
\end{equation} 
Here $p_B = \frac{B^2}{2\mu_0}$ can be identified as the magnetic pressure, such that the first term on the right hand-side becomes the magnetic pressure force similar to the thermal pressure force in equation~\eqref{eq::momentum}. The second term describes the magnetic tension force, acting as a restoring force in response to the deformation of the magnetic field.
%\begin{equation}
%	\mathbf j = \sigma \left[ (\mathbf E + \mathbf u\times \mathbf B) + \frac{1}{ne}\nabla P_e - \frac{1}{ne}\mathbf j\times\mathbf B-\frac{m_e}{ne^2}\left(\frac{\partial\mathbf j}{\partial t}+\nabla\cdot(\mathbf{ju})\right)\right]
%\end{equation}
These equations are accompanied by the simplified Ohm's law,
\begin{equation}\label{eq::ohm}
	\mathbf j = \sigma (\mathbf E + \mathbf u\times \mathbf B)
\end{equation}
which describes the relationship between the electric field and the current density with $\sigma$ as the conductivity.

\begin{wrapfigure}{R}{0.35\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/introduction/masters_2012_mod}
	\caption[Schematic showing the magnetic reconnection process]{Schematic showing the magnetic reconnection process between the \acl{IMF} and magnetospheric field lines, the current sheet highlighted in grey. Modified from \citet{masters_importance_2012}.\\
	}
	\label{fig::intro::reconnection}
\end{wrapfigure}

Combining Ohm's law~\eqref{eq::ohm} with Faraday's law~\eqref{eq::faraday} and Amp\`{e}re's law~\eqref{eq::ampere} yields the induction equation describing the temporal change of the magnetic field,
\begin{equation}\label{eq::induction}
	\frac{\partial \mathbf B}{\partial t} = \nabla \times (\mathbf u \times \mathbf B) + \frac{1}{\mu_0\sigma}\nabla^2\mathbf B.
\end{equation}
The terms on the right hand-side are hereby the convective and diffusive terms, respectively. Their ratio is also known as the Reynolds number, which indicates the relative importance of convection and diffusion to the evolution of the magnetic field. It is given by
\begin{equation}
	R_m = \frac{|\nabla \times (\mathbf u \times \mathbf B)|}{|\nabla^2\mathbf B / \mu_0\sigma|} = \mu_0\sigma u L,
\end{equation}
where $u$ is the characteristic perpendicular velocity of the plasma and $L$ the length scale of variations in $\mathbf B$. In typical collisionless space plasmas with $\sigma \sim \infty$ and very large $L$, we find $R_m \gg 1$ indicating that convection dominates over diffusion. This means that the motion of the plasma and the magnetic field are connected to one another; the magnetic flux is ``frozen in'' to the fluid. As a result of this, particles initially located on the same field line will remain ``attached'' to it as the plasma convects through space.

However, this frozen-in flux approximation breaks down in the process of magnetic reconnection where oppositely directed magnetic fields neighbouring another mix and the field topology is reconfigured. The change in magnetic field across the boundary between two such magnetic fields results in a current flowing within the boundary, forming the current sheet. Compressed by thermal and magnetic pressure, this current sheet becomes thinner, decreasing the characteristic length scale and hence also the Reynolds number until the diffusion term dominates the convection term in equation~\eqref{eq::induction}. The frozen-in flux approximation is broken, and the magnetic field can diffuse from both sides into the current sheet where the oppositely directed field lines essentially break up and reconnect with one another before being ejected from the diffusion region due to magnetic tension forces. The reconnection site in the center of the diffusion region is free of any magnetic field and also known as ``X-line''. A two-dimensional schematic illustrating the theoretical Sweet-Parker geometry of this process \citep{parker_sweets_1957, sweet_neutral_1958} is shown in Figure~\ref{fig::intro::reconnection}.


\subsection{Field-aligned Currents}
Electrical currents are essential for facilitating various coupling mechanisms in planetary magnetospheres, such as momentum coupling through the Lorentz force and electric coupling through induction. Field-aligned currents (\acs{FAC}s) in specific couple the magnetosphere to the ionosphere and are closely related to the generation of auroral emissions.

The deep physical connection between electrical currents and plasma motion precludes a clear identification of cause and effect, but it may be useful to view currents as the effect of plasma motions distorting the magnetic field. From this perspective, the generation of currents can be described by combining Faraday's law~\eqref{eq::faraday} and Amp\`{e}re's law~\eqref{eq::ampere}, neglecting the displacement current in order to exclude high-frequency waves. With the electric field in a fixed frame of reference dominated by $-\mathbf v \times \mathbf B$, one obtains
\begin{equation}
\label{eq::current}
\frac{\partial \mathbf j}{\partial t} = -\frac{1}{\mu_0}\nabla \times \left[\mathbf B (\nabla \cdot \mathbf v) + (\mathbf v \cdot \nabla) \mathbf B - (\mathbf B \cdot \nabla)\mathbf v \right]
\end{equation}
as a general description. Considering small perturbations of an initially uniform magnetic field, an \acs{FAC} can be obtained from the first order terms of \eqref{eq::current} as
\begin{equation}
\label{eq::fac}
\frac{\partial \mathbf j_\parallel}{\partial t} = \frac{1}{\mu_0} (\mathbf B \cdot \nabla) \Omega_\parallel
\end{equation}
with $\Omega_\parallel$ as the field-aligned component of the flow vorticity $\Omega = \nabla \times \mathbf v$. This means that \acp{FAC} can for example be formed by a clockwise twisting of a magnetic flux tube which decreases in the direction of the magnetic field \citep{paschmann_auroral_2003}. A discussion of the generation of \acp{FAC}  specifically in the context of magnetosphere-ionosphere coupling at Saturn can be found in section~\ref{sect::theory::dynamics}.

\subsection{Plasma Waves and Wave-Particle Interactions}

In the typically collisionless space plasmas, waves play an important role for the transfer of energy between different spatial regimes, particles and fields. On Earth we are used to waves occurring in neutral fluids such as air and water, where pressure perturbations excite compressional sound waves. In space plasmas however, the dynamics are additionally connected to electromagnetic fields -- space plasmas can hence support a variety of waves with very different properties. In a magnetized plasma waves can propagate \textit{parallel} or \textit{perpendicular} to the magnetic field, the electric field perturbation they excite can occur \textit{longitudinal} or \textit{transverse} to their wave vector $\mathbf k$, and they may be of an \textit{electromagnetic} or \textit{electrostatic} nature depending on whether they involve a perturbation of the magnetic field or not. 

In a warm \ac{MHD} plasma three different electromagnetic wave modes can be found. One of these is the transverse \textit{shear Alfv\'{e}n} mode whose waves propagate parallel or oblique to the magnetic field with a phase velocity $v_p = v_A\cos\theta$, where $\theta$ is the angle between $\mathbf k$ and $\mathbf B$ and
\begin{equation}\label{eq::}
	v_A = \frac{B}{\sqrt{\mu_0 \rho_m}}
\end{equation}
is the so called Alfv\'{e}n velocity. The magnetic field perturbations associated with shear Alfv\'{e}n waves are perpendicular to both $\mathbf B$ and $\mathbf k$, with magnetic tension acting as the restoring force. This wave mode has no effect on the plasma density/pressure or magnetic field magnitude and is hence dispersionless, but it may act to accelerate auroral particles if the time and length scales of the waves are similar to the basic scales of the plasma \citep[e.g.,][]{saur_waveparticle_2018}. The two other wave modes are the longitudinal \textit{fast magnetosonic} and \textit{slow magnetosonic} modes which carry changes of plasma and magnetic pressure and plasma density.

Other wave modes associated with ion oscillations are \textit{electrostatic ion cyclotron} waves, low-frequency waves which are most intense near the ion gyrofrequencies and their harmonics, and (electrostatic) \textit{ion acoustic waves} which are typically broadband with frequencies up to the ion plasma frequency. Both are longitudinal wave modes, propagating nearly perpendicular and parallel to the magnetic field, respectively. They are typically observed in ion pickup regions and on auroral field lines where field-aligned ion beams are present and energy transfer between waves and local auroral particles may occur \citep[e.g.,][]{kindel_topside_1971, kintner_simultaneous_1979, andre_ion_1987, wahlund_observations_1994}.

\textit{Auroral hiss} emissions are broadband whistler mode waves at frequencies below the electron gyrofrequency ($\sim 10\mathrm{s}-100\mathrm{s}$\,kHz at Saturn) which are right-hand polarized and propagate within a cone angle relative to the magnetic field. They are also observed on auroral field lines at Earth, Jupiter and Saturn, where they are thought to interact with field-aligned electron beams leading to electron scattering and acceleration \citep[e.g.,][]{maggs_coherent_1976, gurnett_auroral_1983, kopf_electron_2010, tetrick_plasma_2017, elliott_pitch_2018}.

Another signature of wave-particle interactions is the occurrence of auroral radio emissions, known as auroral kilometric radiation at Earth, Jovian hectometric radiation at Jupiter and \ac{SKR} at Saturn. They are thought to be generated through the cyclotron maser instability, driven by antiplanetward beams of accelerated electrons in an environment of low-density magnetized plasma such as present in auroral regions, and are observed at frequencies above the local electron gyrofrequency. Remote observation of these auroral radio emissions can provide global insight into auroral and magnetospheric dynamics \citep[e.g.,][]{wu_kinetic_1985, zarka_auroral_1998, louarn_generation_2017, lamy_low-frequency_2018}.


\section{The Heliosphere}
At the center of the solar system, the Sun produces a large amount of energy through fusion of Hydrogen in its core where pressure and temperature are sufficiently high. The so generated energy is transported to the surface and heats the solar corona, essentially the Sun's atmosphere. The high gas pressure arising in this region continuously pushes magnetised coronal plasma away from the Sun, driving the solar wind \citep{parker_dynamics_1958}. The solar wind flow accelerates up to $\sim 400-800$\,km/s within a few solar radii, after which it travels radially outward with relatively constant velocity. It eventually breaks down as it reaches the termination shock, marking the transition from the heliosphere into the heliosheath, beyond which the heliopause is the only separation between the solar system and interplanetary space \citep[e.g.,][]{krimigis_energetic_2019}. The shape of the heliosphere is still uncertain, but recent measurements from the \name{Voyager} and \name{Cassini} spacecraft suggest that it is of bubble-like shape instead of the previously proposed magnetosphere-like shape with an extended tail shaped by the interstellar flow \citep[e.g.,][]{dialynas_bubble-like_2017}. An illustration of this is shown in Figure~\ref{fig::intro::heliosphere}.

%\begin{figure}[tbp]
%	\centering
%	\includegraphics[width=\linewidth]{./img/introduction/dialynas_2017_decol3}
%	\caption[Shape of the heliosphere]{Models for the shape of the heliosphere, (a) a bubble-like and (b) a magnetosphere-like shape with a heliotail. Modified from \citet{dialynas_bubble-like_2017}.}
%	\label{fig::intro::heliosphere}
%\end{figure}

\begin{figure}[tbp]
	\centering
	\includegraphics[width=\linewidth]{./img/introduction/krimigis_2019_comp}
	\caption[Concept of the heliosphere]{Concept of the bubble-like heliosphere based on \name{Voyager} 1 and 2 observations. The trajectories of the two spacecraft are indicated in green and orange; from their departure in the inner solar system passing the termination shock (TS), heliosheath (HS) and heliopause (HP) out of the heliosphere. Taken from \citet{krimigis_energetic_2019}.}
	\label{fig::intro::heliosphere}
\end{figure}

The Sun has a roughly dipolar magnetic field which is thought to be generated by the convection of plasma in the solar interior, acting as a dynamo. Its polarity periodically reverses every $\sim 11$ years, with solar activity decreasing and increasing over the course of this so called solar cycle. One indicator of solar activity is the occurrence of dark spots on the Sun's surface which mark cooler regions featuring intense magnetic field strengths. They are known as sunspots and are typically the origin of transient energetic phenomena such as solar flares and coronal mass ejections. Both sunspot number and location vary characteristically over the course of a solar cycle, as shown in Figure~\ref{fig::intro::solarcycle}. Periods of low solar activity are marked by the near absence of sunspots; sunspots appear at higher latitudes in the inclining phase and at lower latitudes in the declining phase of the solar cycle.

\begin{figure}[tbp]
	\centering
	\includegraphics[width=\linewidth]{./img/introduction/bfly_mod_comp}
	\caption[Sunspot occurrence over time]{The occurrence of sunspots over time. The top panel shows their latitudinal distribution and the bottom panel shows their total area. Image credit: \acs{NASA} / David Hathaway.}
	\label{fig::intro::solarcycle}
\end{figure}

The solar wind plasma is characterized by high conductivities and large length scales, such that the frozen-in flux approximation is satisfied. This means that plasma ejected from the Sun carries with it the Sun's internal magnetic field, then known as \acf{IMF}. With the Sun rotating at a period of $\sim 24.5$\,days at the equator \citep{snodgrass_rotation_1990}, the \ac{IMF} is coiled into a spiral-like configuration as the footpoint of a magnetic flux tube rotates while the associated solar wind plasma moves radially outward. This is known as the Parker spiral \citep{parker_dynamics_1958} and illustrated in Figure~\ref{fig::intro::solarwind}. The spiral angle of the magnetic field, defined as $\arctan(B_{tan}/B_{rad})$ with $B_{tan}$ and $B_{rad}$ as the tangential and radial \ac{IMF} components, respectively, increases with distance from the Sun. While the field is almost radial near the Sun with a Parker spiral angle of $\sim 0^\circ$, the angle is typically $\sim 45^\circ$ at Earth and $\sim 87^\circ$ at Saturn \citep{jackman_overall_2008}. However, this simplified description does not account for the variable tilt of the Sun's magnetic dipole field relative to its rotation axis or for the complex field structure observed during solar maximum.

\begin{figure}[tbp]
	\centering
	\includegraphics[width=\linewidth]{./img/introduction/prolss_solarwind}
	\caption[Structure of the solar wind in the ecliptic]{Structure of the solar wind in the ecliptic, viewed from above the Sun. Dashed lines illustrate the shape of the Parker spiral. Taken from \citet{prolss_physics_2004}.}
	\label{fig::intro::solarwind}
\end{figure}

The solar wind is typically found to exhibit a bimodal velocity structure. Slow solar wind, of the same composition as the solar corona and emerging from the solar equatorial region, has a typical velocity of $\sim 400$\,km/s at the Earth orbit. Fast solar wind is of photospheric composition, is thought to originate from coronal holes and has a velocity of $\sim 750$\,km/s. Fast solar wind may ``catch up'' to slow solar wind, forming a large scale compression also known as \ac{CIR}, also illustrated in Figure~\ref{fig::intro::solarwind}. A \ac{CIR} is typically preceded by a forward shock in the slow solar wind and followed by a reverse shock and a rarefaction region.