\subsection{Global Transport of Mass and Energy}

Most plasma in Saturn's magnetosphere is provided by the moon Enceladus in the inner magnetosphere and lost downtail through magnetic reconnection and the associated plasmoid ejections in the magnetotail. The processes responsible for the transport of plasma between these stages are diverse and the topic of ongoing investigation.

From its origin near Enceladus's orbit, plasma is transported through the magnetosphere mainly by radial interchange. ``Fingers'' of hot and tenuous plasma from the middle magnetosphere are hereby injected into the rather cold and dense plasma of the inner magnetosphere, driven by the centrifugal interchange instability \citep[e.g.,][]{gold_motions_1959, southwood_magnetospheric_1987, thomsen_saturns_2013}. These injections are relatively small with a width of $\sim 1$\,R$_\mathrm S$ and are observed at radial distances between $6-10$\,R$_\mathrm S$, occurring roughly once per day \citep[e.g.,][]{hill_evidence_2005, chen_statistical_2008}. No clear ordering in \ac{LT} has been observed, although some studies suggest that such events may occur more frequently at the nightside \citep{kennelly_ordering_2013, azari_are_2019}.

A second class of injections, unfortunately indistinguishable by name, can be observed at somewhat larger radial distances of $15-20$\,R$_\mathrm S$. These events occur less frequently, but they are larger and more persistent and involve more energetic plasma populations \citep[e.g.,][]{mauk_energetic_2005, paranicas_energetic_2007, carbary_track_2008}. After their appearance typically at Saturn's nightside, their \ac{ENA} signatures can extend across several Saturn radii and several hours \ac{LT} and are observed to rotate around the planet, sometimes more than once, before dispersing \citep[e.g.,][]{mitchell_recurrent_2009, kinrade_tracking_2020}. They appear to be associated with magnetotail activity and are hence likely related to dynamics further out in the magnetosphere.

Large-scale magnetic flux circulation in Saturn's middle and outer magnetosphere is driven by two main sources: the antisunward flow of the solar wind and the rapid rotation of the planet itself. The former drives the Dungey cycle, which is the main transport mechanism at Earth \citep{dungey_interplanetary_1961}; the latter powers the \vasyliunas{} cycle, dominating the Jovian magnetosphere \citep{vasyliunas_plasma_1983}.

\begin{figure}
	\centering
	\raisebox{-0.5\height}{\includegraphics[width=0.49\linewidth]{./img/dynamics/cowley_2004_01}}
	\hfill
	\raisebox{-0.5\height}{\includegraphics[width=0.49\linewidth]{./img/dynamics/cowley_2004_02_ocb}}
	\caption[Sketches of Saturn's equatorial and polar ionospheric plasma flows]{Sketches of (left) plasma flows in Saturn's equatorial magnetosphere and (right) plasma flows and field aligned currents in Saturn's polar ionospheres. Both sketches describe the system seen from above the north pole and with the Sun toward the bottom. Plasma flows are indicated with solid arrowed lines, while boundaries between different flow regimes are shown with dashed lines. The red circle marks the location of the auroral oval and the \ac{OCB}. Upward and downward current regions are indicated with circled dots and crosses, respectively. Modified from \citet{cowley_saturns_2004}.}
	\label{fig::dyn::flows}
\end{figure}

In the Dungey cycle process \citep{dungey_interplanetary_1961}, magnetospheric flux tubes reconnect with the oppositely directed \ac{IMF} at the dayside magnetopause, creating open flux tubes whose ends are locked in the solar wind. These are dragged over the polar caps by the solar wind flow, stretching them and depositing them in the long magnetotail. Meanwhile, the flux tubes' ionospheric ends move into the polar cap to join preexisting regions of open flux and eventually are carried into the nightside as additional open flux is added to the dayside and eroded from the nightside polar cap. The magnetic field, directed oppositely on the two sides of the magnetotail current sheet, can then reconnect again. This leads to magnetic flux and plasma being ejected downtail, while the newly closed magnetospheric flux tubes contract toward the planet into a more dipolar configuration due to magnetic tension (``magnetic dipolarization''). The flux tubes, now empty of magnetic flux, then flow back to the dayside to begin the described process anew.

The Dungey cycle is the main process of flux circulation at Earth, but it is of much smaller importance for the rotating giant planet magnetospheres of Jupiter and Saturn. With a magnetic field strength ten times larger than that of Earth, a magnetosphere nearly a hundred times as voluminous as Earth's and a rotation period of less than ten hours, especially Jupiter's magnetosphere is almost entirely controlled by its internal dynamics \citep{brice_magnetospheres_1970}.

Global plasma circulation is here described with the \vasyliunas{} cycle \citep{vasyliunas_plasma_1983}: Magnetospheric plasma is added internally by ionization of cryovolcanic ejecta from the moon Io \citep[e.g.,][and references therein]{thomas_io_2004}. This continuous mass and momentum loading stretches the magnetic field into a disc shape, facilitating reconnection through the thin equatorial current sheet as a result of rotational stresses and the related plasma instabilities \citep{kivelson_dynamical_2005}. Plasma is released, the magnetic field dipolarizes and the remaining plasma continues to subcorotate. As the solar wind limits the outward expansion of the magnetosphere at the dayside, the magnetodisc is considered thicker than in the magnetotail - \vasyliunas{}-type reconnection is therefore suggested to occur mainly at the nightside. However, recent investigations at Saturn have proposed this process to be active at all local times \citep{delamere_magnetic_2015} and in-situ signatures of dayside magnetodisc reconnection have also been observed \citep{guo_reconnection_2018, guo_rotationally_2018}.

Saturn's magnetosphere is thought to be controlled by a combination of these two processes \citep{cowley_simple_2004, cowley_saturns_2004}; their relative importance varies depending on, e.g., solar wind conditions and the internal plasma loading rate. It has been suggested that Dungey cycle reconnection voltages are comparable to \vasyliunas{} cycle voltages under strong solar wind driving \citep{jackman_model_2006}, but less significant during periods of low to average solar wind activity \citep{badman_significance_2007}. Figure~\ref{fig::dyn::flows} shows sketches of the expected plasma flows in Saturn's equatorial magnetosphere and polar ionospheres resulting from a combination of the Dungey and \vasyliunas{} cycles. Both are associated with their own ionospheric flow regions; the Dungey cycle dominating the polar cap region and the \vasyliunas{} cycle controlling the corotating plasma flow just equatorward of the \acf{OCB}.

As a result, the polar cap ionosphere is found to rotate at angular velocities of only $\sim 30-50\%$ of rigid corotation \citep[e.g.,][]{stallard_ion_2004}, transferring angular momentum to the tail by twisting the tail lobe field lines \citep{isbell_magnetospheric_1984}. With the closed magnetosphere just equatorward of the \ac{OCB} rotating much more rapidly, a shear in ionospheric plasma flows is maintained which is thought to set up a system of \acp{FAC} driving Saturn's main aurorae \citep[e.g.,][]{cowley_simple_2004, stallard_saturns_2007, talboys_signatures_2009, hunt_field-aligned_2014, bradley_field-aligned_2018}.

\subsection{Corotation Breakdown and Magnetosphere-Ionosphere Coupling}

\begin{wrapfigure}{R}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/dynamics/thomsen_2010}
	\caption[Azimuthal ion flow speeds in Saturn's equatorial magnetosphere]{Azimuthal flow speeds for three different ion species versus L shell; the values were derived from \name{Cassini} \ac{CAPS} measurements close to the equatorial plane. Solid lines indicate rigid corotation. Taken from \citet{thomsen_survey_2010}.}
	\label{fig::dyn::azim_flows}
\end{wrapfigure}

Most plasma is confined close to the magnetic equator by the centrifugal force, forming the magnetodisc \citep[e.g.,][]{persoon_simple_2006}; especially heavier ions are concentrated at very low latitudes \citep[e.g.,][]{thomsen_survey_2010, persoon_plasma_2013}. The confined plasma largely flows in the direction of corotation \citep[e.g.,][]{thomsen_survey_2010, thomsen_plasma_2014, livi_multi-instrument_2014}, but the flow speeds are generally below rigid corotation speed as shown in Figure~\ref{fig::dyn::azim_flows}. Full corotation is observed until $\sim$3.3\,R$_\mathrm S$, while the plasma begins to lag the planet's angular velocity further outward \citep[e.g.,][]{wilson_thermal_2009}. The subcorotation of the plasma relative to the neutral atmosphere is a direct consequence of mass and momentum loading and the outward transport of plasma under conservation of angular momentum \citep[e.g.,][]{hill_inertial_1979, pontius_plasma_2009}.

The angular flow shear arising from this differential plasma corotation sets up a current system which acts to transfer angular momentum from the planet to the surrounding magnetospheric plasma. A brief description will be given below, but the reader is referred to other works for more detailed discussions on this topic \citep[e.g.,][]{hill_inertial_1979, hill_jovian_2001, cowley_origin_2001, cowley_corotation-driven_2003, ray_magnetosphere-ionosphere_2010, ray_local_2014, ray_auroral_2012, vasyliunas_physical_2016, ray_magnetospheric_2018}.

Newly ionized plasma orbits Saturn at the Keplerian velocity, which is lower than rigid subcorotation. Upon its ionization, the bulk flow speed of the local plasma population hence decreases and a motional electric field is generated. In the corotating frame, this field points radially in toward the planet ($E_M$ in Figure~\ref{fig::dyn::mi_coupling}); in the ionosphere, this relates to an equatorward pointing latitudinal field ($E_I$).

The ionospheric field is associated with latitudinal Pedersen currents, which are fed by \acp{FAC} diverging into the magnetodisc. The upward current is hereby carried by electrons precipitating into the ionosphere; most assuredly generating the steady main aurorae at Jupiter \citep[e.g.,][]{cowley_origin_2001} but likely too weak to produce auroral emissions at Saturn \citep{cowley_corotation-driven_2003}.

\begin{wrapfigure}{R}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/dynamics/ray_magnetosphere_2010}
	\caption[Magnetosphere-ionosphere coupling diagram]{Diagram of the magnetosphere-ionosphere coupling currents and fields. $E_I$ and $E_M$ are the perpendicular electric fields in the ionosphere and magnetosphere, respectively. $K_I$ denotes the ionospheric Pedersen current and $K_M$ the radial current in the magnetodisc. The field-aligned potential between the ionosphere and magnetosphere is given by $\Phi_\parallel$, the \ac{FAC}s by $J_\parallel$. Modified from \citet{ray_magnetosphere-ionosphere_2010}.}
	\label{fig::dyn::mi_coupling}
\end{wrapfigure}

The current circuit is closed by electrons flowing radially outward in the magnetodisc \citep[e.g.,][]{martin_current_2019} and back into the ionosphere along the magnetic field. Radial currents in the magnetodisc are associated with a $\mathbf J \times \mathbf B$ force which, for the case of outward directed currents, acts to accelerate the subcorotating plasma. The angular momentum for this azimuthal acceleration is supplied from the planet's rotation, transferred into the ionosphere through ion-neutral collisions and from there into the magnetosphere through the magnetosphere-ionosphere coupling current system described here. However, this current system is not able to enforce rigid corotation throughout the magnetosphere as it is limited by, e.g., the conductance of the ionosphere and the density of current carriers along the magnetic field lines \citep[e.g.,][]{ray_auroral_2012, ray_magnetospheric_2018}.

\subsection{Rotational Modulation}

\ac{SKR} measurements from the first Saturn flybys of Voyager~1 and 2 revealed periodic oscillations which were assumed to represent the planetary rotation period \citep{desch_voyager_1981}. However, further observations unexpectedly showed the \ac{SKR} period to vary by $\sim 1^\circ$ over several years \citep{galopeau_variations_2000}; and the first \name{Cassini} \ac{SKR} observations indeed verified a shift in the period from $\sim 10.66$\,h to $\sim 10.76$\,h \citep{gurnett_radio_2005}. The puzzling appearance of a secondary period \citep{kurth_update_2008} later led to the discovery of separate periods corresponding to \ac{SKR} sources in the northern and southern hemisphere, respectively \citep{gurnett_discovery_2009}. These can be separated by the circular polarization of the extraordinary mode emission, with left-hand-polarized \ac{SKR} being generated in the southern and right-hand-polarized \ac{SKR} in the northern hemisphere \citep[e.g.,][]{lamy_saturn_2008}. Both the northern and the southern period vary slowly over time (see Figure~\ref{fig::dyn::skr_time}); at \name{Cassini}'s arrival the two were clearly separated, but with the approach of Saturn's equinox they became more similar and eventually seemed to merge \citep{gurnett_reversal_2010}, suggesting a seasonally dependent driver \citep{brooks_saturns_2019}.

\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/dynamics/desch_1981}
	\caption[Voyager SKR power spectra]{Power spectra of Voyager \ac{SKR} time series, (top) high-resolution and (bottom) low-resolution. Taken from \citet{desch_voyager_1981}.}
	\label{fig::dyn::voyager_skr}
\end{wrapfigure}

These so-called \acf{PPO} periodicities and their evolution through time can also be tracked using magnetic field measurements from within Saturn's magnetosphere \citep[e.g.,][]{espinosa_periodic_2000, giampieri_regular_2006}. The specific phase relation between the magnetic components hereby precludes that the periodicities may be generated by a tilted magnetic dipole \citep{espinosa_reanalysis_2003}; instead, magnetic perturbation fields were found to model the magnetic \ac{PPO} perturbations rather closely.

\begin{wrapfigure}{R}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/dynamics/gurnett_2010_cmod}
	\caption[SKR period evolution from \name{Voyager} to \name{Cassini}]{(a) Saturnian latitudes of the Sun and \name{Ulysses}, (b) \ac{SKR} periods over time as measured by the \name{Voyager} 1 and 2, \name{Ulysses} and \name{Cassini} spacecraft. Modified from \citet{gurnett_reversal_2010}.}
	\label{fig::dyn::skr_time}
\end{wrapfigure}

Both the northern and southern \ac{PPO} systems are associated with one perturbation field each, dominant in their respective polar hemisphere but double-modulating the magnetic field in the equatorial plane. Their perturbation dipoles are hereby oriented perpendicular to Saturn's rotation axis and rotate with their respective \ac{PPO} period; they are associated with a complex array of \acp{FAC} \citep[e.g.,][]{southwood_saturnian_2007, andrews_planetary_2008, andrews_magnetospheric_2010, andrews_magnetic_2010, andrews_planetary_2012, provan_polarization_2009, provan_magnetospheric_2011, southwood_origin_2014, hunt_field-aligned_2014, hunt_field-aligned_2015, bradley_field-aligned_2018}.

The origin of Saturn's periodicities is a matter of ongoing investigation. Global \ac{MHD} simulations could model the observed double modulation of the magnetosphere with vortical flow structures in Saturn's polar ionospheres as a driver \citep{jia_driving_2012-1, jia_driving_2012, kivelson_control_2014}. The proposed system is powered by double vortices in the polar caps which rotate at the observed \ac{PPO} periods; the \acp{FAC} associated with the \ac{PPO} systems were shown to flow in the right sense to communicate the related modulations out into the magnetosphere \citep[e.g.,][]{hunt_field-aligned_2014, southwood_origin_2014}. The \ac{PPO}-associated \acp{FAC} close partly in the equatorial plane and partly in the opposite hemisphere, pervading the entire magnetosphere \citep[e.g.,][]{southwood_saturnian_2007, southwood_source_2009, hunt_field-aligned_2015, bradley_field-aligned_2018}. A sketch of \ac{PPO}-related magnetic field perturbations and electric currents in accordance with the currently accepted model is shown in Figure~\ref{fig::dyn::provan_2018}. However, it is unclear how these ionospheric flow patterns could be generated - although it is likely that the driver may be unconnected with magnetospheric processes and instead be found in the neutral thermosphere or stratosphere \citep[e.g.,][]{smith_saturnian_2011, smith_nature_2014, stallard_saturns_2018}.

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{./img/dynamics/provan_2018_proj}
	\caption[Sketch of PPO-related magnetic field perturbations and electric currents]{PPO-related magnetic field perturbations and electric currents of the (a-c) northern and (d-f) southern \ac{PPO} systems. Red arrowed lines and symbols show electric currents and blue arrowed lines and symbols magnetic fields, with circled dots and crosses indicating vectors pointing out of and into the plane of the figure, respectively. (a) View onto Saturn's northern polar ionosphere and (d) view from north ``through'' the planet into the southern ionosphere, yellow arrowed lines indicating the atmospheric/ionospheric flow patterns thought to generate \acp{PPO}. (b,e) Fields and currents in the $\Psi_\mathrm{N,S}=90^\circ \: \textrm{---}\: 270^\circ$ meridian plane and (c,f) fields and currents in the $\Psi_\mathrm{N,S}=0^\circ \: \textrm{---}\: 180^\circ$ meridian plane. Detailed descriptions of \ac{PPO} phase angles and longitude systems are given in section~\ref{sect::instr::ppo}. The background magnetic field is indicated in black. Modified from \citet{provan_planetary_2018}.}
	\label{fig::dyn::provan_2018}
\end{figure}

The effects of this periodic modulation prevail in virtually all measurements within Saturn's magnetosphere. As already mentioned, rotational modulation is observed in magnetic field measurements \citep[e.g.,][]{
espinosa_periodic_2000,
espinosa_reanalysis_2003,
andrews_planetary_2008,
andrews_magnetic_2010,
andrews_magnetospheric_2010,
andrews_planetary_2012,
provan_polarization_2009,
provan_magnetospheric_2011,
provan_planetary_2013,
provan_planetary_2014,
provan_planetary_2015,
provan_planetary_2016,
provan_planetary_2018}
as well \ac{SKR} variations \citep[e.g.,][]{
lamy_variability_2011,
lamy_multispectral_2013,
ye_rotational_2016}.
The \acp{FAC} flowing on auroral field lines connect the entire magnetosphere with the supposed atmospheric drivers in Saturn's polar ionospheres, and hence show clear \ac{PPO} periodicities as well \citep[e.g.,][]{
hunt_field-aligned_2014,
hunt_field-aligned_2015,
hunt_field-aligned_2016,
hunt_field-aligned_2018,
hunt_saturns_2018,
bradley_field-aligned_2018}.
The \ac{PPO}-associated variation of the current density on these field lines manifests as a periodic modulation of the auroral intensity \citep[e.g.,][]{
nichols_variation_2010,
badman_rotational_2012,
nichols_saturns_2016,
bader_statistical_2018}
and of the latitude of the main emission in phase with the \ac{PPO} modulation \citep[e.g.,][]{
provan_phase_2009,
nichols_dawn-dusk_2010,
nichols_saturns_2016,
bader_modulations_2019}.
The oscillations introduced by these rotating current systems propagate through the entire magnetosphere, such that the nightside plasma sheet can be observed to flap about the magnetic equator and periodically vary in thickness \citep[e.g.,][]{
arridge_periodic_2011,
provan_dual_2012,
cowley_planetary_2017,
thomsen_evidence_2017,
sorba_periodic_2018}.
At its thinnest, the plasma sheet is found more conducive to reconnection; plasmoid release and magnetic dipolarizations are hence more probable at certain \ac{PPO} phases \citep[e.g.,][]{
jackman_reconnection_2016,
cowley_planetary_2017,
bradley_planetary_2018,
bader_dynamics_2019}.
These global magnetospheric periodicities also emerge in the variation of the magnetopause location \citep{
clarke_magnetopause_2010}, 
the bow shock location \citep{
clarke_magnetospheric_2010}
and the \ac{OCB} \citep{
jasinski_saturns_2019}.
Finally, comparable periodicities are observed in all kinds of measurements of charged particles and \acp{ENA} \citep[e.g.,][]{
paranicas_periodic_2005,
carbary_ena_2008,
carbary_ena_2011,
carbary_update_2017}
and even in the structure of Saturn's rings \citep{
chancia_seasonal_2019}.