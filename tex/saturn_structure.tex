\begin{figure}[b!]
	\centering
	\includegraphics[width=\linewidth]{./img/structure/saturn_magnetosphere_nasa_bw_comp}
	\caption[Global configuration of Saturn's magnetosphere]{Detailed illustration of the global configuration of Saturn's magnetosphere. Image credit: \acs{NASA}/\acs{JPL}.}
	\label{fig::struct::saturn_magnetosphere}
\end{figure}


Saturn's magnetosphere is the second largest magnetosphere in our solar system. After several flyby missions, \name{Cassini} was the first and until now only spacecraft orbiting Saturn (2004-2017) and performing extensive observations of its plasma environment.

The magnetosphere, an illustration of which is shown in Figure~\ref{fig::struct::saturn_magnetosphere}, is formed as the solar wind is countered by Saturn's intrinsic magnetic field and locally produced plasma, representing an obstacle in the steady solar wind flow. The solar wind plasma is slowed down and diverted around the magnetosphere, separated from Saturn's magnetospheric plasma by the magnetopause. Magnetospheric neutrals and plasma are largely provided by the moon Enceladus with its numerous geysers, releasing water vapor and ice into Saturn's inner magnetosphere. Outside of Enceladus' orbit, in the middle magnetosphere, the ejected material mass- and momentum-loads the system and the plasma angular velocity begins to depart from rigid corotation. This region also includes the ring current. The subcorotating plasma takes the shape of a magnetodisc, stretching the magnetic field in the equatorial plane. The outer magnetosphere, beyond Titan's orbit, is dominated by highly stretched field lines and a thin hinged magnetodisc. Quasi-periodic magnetotail reconnection of the nightside magnetodisc is thought to control the plasma outflow, balancing the plasma loading from Enceladus. This section reviews the current understanding of the structure of Saturn's magnetosphere and details its most relevant components.


\subsection{The Internal Magnetic Field}
Saturn's magnetic field was first investigated during the \name{Pioneer 11} flyby in 1979 \citep{smith_saturns_1980, acuna_magnetic_1980}, and soon after during the two \name{Voyager} flybys in 1980/1981 \citep{connerney_zonal_1982}. These observations showed a close alignment between the dipolar magnetic field and the planet's spin axis as well as a significant quadrupole moment, describing a northward shift of the magnetic equator from the planetary equator. 

With the arrival of \name{Cassini} at Saturn, the magnetic field could be characterized more accurately - data obtained during the Grand Finale phase of the \name{Cassini} mission revealed a dipole tilt $<0.0095^\circ$ and a $0.0466\,\mathrm R_S = 2808\,\mathrm{km}$ northward shift of the magnetic dipole \citep{dougherty_saturns_2018}. There is no evidence of longitudinal variation in the field \citep{cao_saturns_2011}, preventing an accurate determination of the planet's rotation rate.

The perfect axisymmetry of Saturn's internal magnetic field is puzzling, as Cowling's theorem precludes the generation of an axisymmetric field through a dynamo process \citep{cowling_magnetic_1933}. A widely accepted explanation for this non-conformity proposes that nonaxisymmetric magnetic moments are electromagnetically shielded by differential rotation in an electrically conducting layer above the deep dynamo region \citep{stevenson_saturns_1980, stevenson_reducing_1982}. \name{Cassini}'s Grand Finale measurements revealed latitudinally banded magnetic fields, supporting this theory \citep{dougherty_saturns_2018}.

Saturn's internal field $\mathbf B$ outside of the dynamo region is modelled as the gradient of a magnetic scalar potential $V$,
\begin{equation}
	\mathbf B = -\nabla V,
\end{equation}
assuming that the field is approximately curl-free (i.e., the effects of external current systems are negligible). This potential is then
\begin{equation}
	V(r, \theta, \phi) = \sum_{n=1}^{\infty} \sum_{m=0}^{n} R_\mathrm S \left(\frac{R_\mathrm S}{r}\right)^{n+1}
	P_n^m(\cos\theta)\left[g_n^m\cos(m\phi) + h_n^m\sin(m\phi)\right],
\end{equation}
with $R_\mathrm S$ as the radius of Saturn and $r$, $\theta$ and $\phi$ as the spherical planetocentric coordinates. $P_n^m(\cos\theta)$ are hereby Schmidt-normalised Legendre polynomials, while $g_n^m$ and $h_n^m$ are Gauss coefficients describing the contribution of each spherical mode to the field.

The values of $n$ and $m$ indicate the degree and order of the spherical harmonic field expansion, respectively. The Gauss coefficients of the most recent ``\name{Cassini} 11'' field model are given in Table~\ref{tbl::cassini11_field}. In spherical planetocentric coordinates,
\begin{equation}
	B_r=-\frac{\partial V}{\partial r}, \quad B_\theta = -\frac{1}{r}\frac{\partial V}{\partial \theta}, \quad B_\phi = - \frac{1}{r\sin\theta}\frac{\partial V}{\partial \phi}
\end{equation}
gives the magnetic field components corresponding to the modelled potential $V(r, \theta, \phi)$.

\subsection{Enceladus: The Main Source of Magnetospheric Plasma}

Enceladus is a small moon ($\sim 500$\,km diameter) located in Saturn's inner magnetosphere at a radial distance of $\sim 4\,R_\mathrm S$. The first flybys performed by \name{Cassini} in 2005 revealed significant cryovolcanic activity - dozens of distinct water-rich jets were found to erupt from the moon's southern polar terrain (see Fig.~\ref{fig::struct::enceladus}) where ice grains and water vapor are ejected from four prominent surface fractures, dubbed ``tiger stripes'' \citep[e.g.,][]{dougherty_identification_2006, hansen_enceladus_2006, porco_cassini_2006}. The cause of Enceladus' dynamic outgassing is thought to be the tidal deformation and energy dissipation arising from its orbital resonance with the moon Dione, upholding the eccentricity of Enceladus' orbit and causing diurnal variations in the magnitude and direction of the tidal distortion. This energy input likely maintains a global or regional subsurface ocean, providing ejecta and enhancing the tidal stresses imposed on the moon's icy shell \citep[e.g.,][]{hedman_observed_2013, porco_how_2014, patthoff_implications_2019}.

\begin{wraptable}{R}{0.5\textwidth}	
	\centering
	\vspace{-1\baselineskip}
	\caption[Gauss coefficients of the \name{Cassini 11} internal magnetic field model]{Gauss coefficients of the \name{Cassini 11} internal magnetic field model, derived from \name{Cassini} Grand Finale magnetic field measurements \citep{dougherty_saturns_2018}}
	\label{tbl::cassini11_field}	
	\vspace{0.3\baselineskip}
	\renewcommand{\arraystretch}{0.92}
	\begin{tabular}{c|c|c}
			%\renewcommand{\arraystretch}{1}
			\toprule
			 & Value (nT) & Uncertainty (nT) \\ \midrule
			
			$g_1^0$ & 21140.2 & 1.0 \\
			$g_2^0$ & 1581.1 & 1.2 \\
			$g_3^0$ & 2260.1 & 3.2 \\
			$g_4^0$ & 91.1 & 4.2 \\
			$g_5^0$ & 12.6 & 7.1 \\
			$g_6^0$ & 17.2 & 8.2 \\
			$g_7^0$ & -59.6 & 8.1 \\
			$g_8^0$ & -10.5 & 8.7 \\
			$g_9^0$ & -12.9 & 6.3 \\
			$g_{10}^0$ & 15.0 & 7.0 \\
			$g_{11}^0$ & 18.2 & 7.1 \\\bottomrule
	\end{tabular}
\end{wraptable}

The atmospheric plumes are dominated by water and contain significant amounts of carbon dioxide, an unidentified species with a mass-to-charge ratio of 28 (either carbon monoxide or molecular nitrogen), and methane \citep{waite_cassini_2006}. The total mass flux is estimated at $\sim 200$\,kg/s for water vapor \citep{hansen_composition_2011} and $\sim 50$\,kg for water ice \citep{ingersoll_total_2011}, although the emission rates were found to be highly variable over time \citep[e.g.,][]{saur_evidence_2008, smith_enceladus_2010} and the solid-to-gas ratio varies between different geysers \citep{hedman_spatial_2018}. The ejected particles easily escape Enceladus' weak gravitational field and populate a neutral torus \citep{johnson_enceladus_2006} and Saturn's E-ring \citep{spahn_cassini_2006, mitchell_tracking_2015}.

\begin{figure}
	\centering
	\includegraphics[height=0.375\linewidth]{./img/structure/porco_2014_02}
	\hfill
	\includegraphics[height=0.375\linewidth]{./img/structure/porco_2014_new_red}
	\caption[Water plumes on Enceladus]{Water plumes on Enceladus. (Left) Mosaic of \name{Cassini}'s \ac{ISS} images of the south polar terrain, at a resolution of $\sim 80$\,m/pixel and (right) Basemap of the south polar terrain, showing the ``tiger stripes''. Observed jet sources are marked with colored circles. Modified from \citet{porco_how_2014}.}
	\label{fig::struct::enceladus}
\end{figure}

Only a minority of the produced neutrals will eventually be ionized. Neutrals can be directly ionized by either electron impact ionization or photoionization, adding new ions and mass-loading the plasma environment. Charge exchange (neutral-ion collisions) on the other hand keeps the ion population constant and instead momentum-loads the system as the newly ionized particles are generally moving slower than the surrounding plasma \citep[e.g.,][]{blanc_saturn_2015} and the neutrals created in this process escape the magnetic field unimpeded as \acp{ENA}. The fraction of neutrals which is converted into plasma ranges between $17-38\%$ \citep[e.g.,][]{jurac_self-consistent_2005, cassidy_collisional_2010}; Enceladus hence provides plasma at a rate of $12-250$\,kg/s \citep{bagenal_flow_2011} if a neutral production rate of $70-750$\,kg/s \citep{smith_enceladus_2010} is assumed.

\subsection{Radiation Belts}

%\begin{figure}
%	\centering
%	\includegraphics[width=\linewidth]{./img/structure/radbelts_science_2018_bw}
%	\caption[Saturn's proton radiation belts]{Saturn's proton radiation belts, located between the rings and moons as indicated. Modified from \citet{roussos_radiation_2018}.}
%	\label{fig::struct::radbelts_nature}
%\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{./img/structure/radbelts_krupp_2018}
	\caption[Saturn's proton and electron radiation belts]{Differential intensities of energetic protons (black, labelled with ``p'') and electrons (grey, labelled with ``e'') versus L-shell. The intensities shown are median averages from the entire \name{Cassini} mission, obtained from the \name{Cassini} \ac{MIMI} instrument during times when \name{Cassini} passed the equatorial region. The energy ranges and instrument channels considered are given next to the graphs. Taken from \citet{krupp_global_2018}.}
	\label{fig::struct::radbelts_krupp}
\end{figure}

% radiation belts
Saturn's inner and middle magnetosphere is dominated by ion and electron radiation belts (see Figure~\ref{fig::struct::radbelts_krupp}) which have first been observed during the \name{Pioneer 11} flyby \citep{fillius_trapped_1980}. The main radiation belts extend from the outer edge of the F-ring (2.3\,$R_\mathrm S$) to roughly the orbit of the moon Tethys (4.9\,$R_\mathrm S$); they are clearly separated from one another by the major moon orbits (Janus at 2.5\,$R_\mathrm S$, Mimas at 3.1\,$R_\mathrm S$, and Enceladus at 4.0\,$R_\mathrm S$) as the moons remove charged particles while they orbit close to the equatorial plane \citep[e.g.,][]{krupp_energetic_2009, roussos_long-_2011}. Due to this separation, the main radiation belts are very stable as they are to some degree protected by external dynamics. However, between the orbits of Tethys and Dione (6.4\,$R_\mathrm S$) an additional transient radiation belt has been observed to appear in response to interplanetary events caused by solar eruptions \citep{roussos_discovery_2008}.

The formation process of Saturn's ion radiation belts is not entirely understood. At least the high energy part of the ion distribution in the main belts and the small but energetic D-ring radiation belt \citep{roussos_radiation_2018} are thought to originate from the \ac{CRAND} process \citep{kollmann_processes_2013} - galactic cosmic rays impact material in Saturn's atmosphere, moons and rings, and cause energetic secondary neutrons to be emitted into space, which then decay into protons and electrons populating the radiation belts. This is supported by observations showing that the proton radiation belt intensity follows the solar cycle \citep{roussos_long-_2011}, as the access of galactic cosmic rays to Saturn's magnetosphere and hence also the \ac{CRAND} process are directly related to solar activity. Recent modelling also proposed in-situ acceleration through wave-particle interaction as a possible source of trapped electron radiation belts \citep{woodfield_formation_2018}.

\subsection{Ring Current}

Outside of about 6\,$R_\mathrm S$, Saturn's magnetic field configuration starts to become more stretched due to the existence of an azimuthal ring current. This has been observed in magnetic field and particle data from the \name{Pioneer 11} and \name{Voyager} flybys \citep[e.g.,][]{smith_saturns_1980-1, connerney_saturns_1981, ness_magnetic_1981, ness_magnetic_1982, krimigis_general_1983} and was investigated in more detail during the \name{Cassini} era \citep[e.g.,][]{bunce_cassini_2007, bunce_magnetic_2008, kellett_nature_2010, kellett_saturns_2011, sergis_particle_2010}.

The ring current is carried by magnetospheric ions and electrons drifting azimuthally around the planet at different speeds and in different directions depending on their charge. In Saturn's rapidly rotating nonuniform magnetospheric field, this drift occurs due to the single particle curvature drift and the inertia drift associated with plasma corotation with the planet. The magnetic perturbation field induced by this current points northward at the inner edge of the ring current and southward at the outer edge. This reduces the magnetic field close to the planet but increases it in the outer magnetosphere, essentially stretching the planetary magnetic field lines away from the planet in the equatorial plane \citep[e.g.,][]{gombosi_saturns_2009, sergis_ring_2018}. The ring current statistically carries $9.2\pm 1.0$\,MA in total and maximizes at a radial distance of $\sim 9.5\,R_\mathrm S$, although it can extend from $\sim 3-20\,R_\mathrm S$ with an estimated half-thickness of $\sim 1.5\,R_\mathrm S$ \citep{carbary_statistical_2012}.

It is worth pointing out that the ring current is highly dynamic and often asymmetric. Imaging of \acp{ENA}, which allows for remote observations of the global equatorial ring current from above or below the planet, revealed that the ring current frequently exhibits distinct and persistent azimuthal intensity variations \citep[e.g.,][]{krimigis_dynamic_2007, carbary_statistical_2008}. Furthermore, the asymmetric ring current has been shown to be enhanced in a periodic manner at roughly the planetary rotation period \citep[e.g.,][]{paranicas_periodic_2005, carbary_ena_2008, mitchell_recurrent_2009}.


\subsection{Magnetodisc and Magnetotail}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/structure/arridge_2008}
	\caption[Distortion of Saturn's magnetodisc]{Schematic illustrating the distortion of Saturn's magnetodisc. (Left) View of the noon-midnight meridian and (right) three-dimensional illustration of the bowl-shaped current sheet. Taken from \citet{arridge_warping_2008}.}
	\label{fig::struct::magnetodisc_bowl}
\end{figure}

In the outer magnetosphere, the magnetic field is distorted into a non-dipolar current sheet configuration as the magnetic tension cannot maintain balance with the outward plasma pressure and centrifugal forces. Newly added plasma, centrifugally confined to the equatorial region, is driven outward and the plasma corotation with the planet breaks down. Consequently, the closed magnetic field lines become highly stretched and bent back azimuthally due to the increasing subcorotation of the plasma populations in the outer magnetosphere, forming the magnetodisc \citep[e.g.,][]{vasyliunas_plasma_1983, arridge_saturns_2008}. Due to the steady flow of the solar wind, the magnetodisc is displaced from the rotational equator such that it takes the shape of a bowl \citep{arridge_warping_2008}, as is illustrated in Figure~\ref{fig::struct::magnetodisc_bowl}.

The extent of the magnetodisc is on the dayside limited by the magnetopause standoff distance, as the solar wind pressure counters the outward centrifugal forces acting on the plasma in the magnetodisc current sheet. The location of Saturn's magnetopause was found to be highly variable depending on both the internal plasma pressure and the solar wind conditions, varying by several Saturn radii around roughly $25\,R_\mathrm S$ \citep[e.g.,][]{arridge_modeling_2006, achilleos_large-scale_2008, pilkington_internally_2015}. On the nightside however, solar wind drag acts to elongate the already stretched magnetosphere to much larger distances from Saturn, forming the magnetotail. The magnetic field lines become parallel, pointing radially outward above the current sheet and radially inward below. Outside the current sheet are the lobes, containing the open flux of the system - magnetic field lines connected to Saturn's ionosphere on one end and to the solar wind magnetic field on the other \citep[e.g.,][]{gombosi_saturns_2009}.




