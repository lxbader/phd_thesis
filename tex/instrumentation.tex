%==================
\section{The Cassini-Huygens Mission}
\label{sect::inst::overview}
% general information
The \name{Cassini-Huygens} mission was a flagship-class planetary science mission to the Saturnian system, a collaboration between the \ac{NASA}, the \ac{ESA}, and the \name{Agenzia Spaziale Italiana} (ASI). The project comprised both \acs{NASA}'s \name{Cassini} orbiter and \ac{ESA}'s \name{Huygens} probe, which entered the atmosphere of Saturn's biggest moon Titan and descended to its surface via parachute.

% mission timeline
Following its launch in October 1997, the spacecraft performed flybys of Venus, Earth, and Jupiter in order to gain enough momentum to reach Saturn. \name{Cassini-Huygens} was inserted into Saturn orbit on the 1st of July 2004 after more than six years of cruise time through the solar system. The primary mission was completed in 2008, and several mission extensions ensured continued scientific investigations during the \textit{Equinox mission} (until September 2010), \textit{Solstice mission} (until April 2017), and the \textit{Grand Finale mission} (until September 2017). On the 15th of September 2017, the extraordinarily productive project ended with \name{Cassini}'s controlled plunge into Saturn's atmosphere.

\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{./img/instrumentation/78_Cassini06_crop_mark_comp}
	\caption[Schematic of the \name{Cassini-Huygens} spacecraft]{Schematic of the \name{Cassini-Huygens} mission, most of the key components are labelled. Instruments of specific interest in the context of this thesis are highlighted in red. Image credit: \acs{NASA}/\acs{JPL}.}
	\label{fig::instr::cassini_sketch}
\end{figure}

% spacecraft properties
A schematic of the \name{Cassini} spacecraft is shown in Figure \ref{fig::instr::cassini_sketch}. \name{Cassini} was a three-axis stabilized robotic spacecraft powered by three \acp{RTG}. With a launch mass of nearly 6 tonnes, it was one of the largest interplanetary spacecraft ever built. The \name{Cassini} probe was equipped with twelve interrelating science instruments that addressed many major scientific questions about the Saturn system. The remote sensing pallet comprised the following instruments used mainly for optical imaging in different wavelength ranges:
\begin{itemize}
	\item \textbf{CIRS} (\name{Composite Infrared Spectrometer}) measured infrared emissions from atmospheres, rings and surfaces and analyzed their temperature and composition \citep{flasar_exploring_2004}
	\item \textbf{\acs{ISS}} (\acl{ISS}) was a camera system operating in the visual and infrared wavelength range and photographing Saturn, its rings and its moons \citep{porco_cassini_2004}
	\item \textbf{\acs{UVIS}} (\acl{UVIS}) imaged Saturn's rings and auroral emissions \citep{esposito_cassini_2004}
	\item \textbf{VIMS} (\name{Visible and Infrared Mapping Spectrometer}) mapped the surface spatial distribution of mineral and chemical features of different targets in high spectral resolution \citep{brown_cassini_2004}
\end{itemize}
A second group of instruments was dedicated to studying dust and plasma in the environment of Saturn:
\begin{itemize}
	\item \textbf{\acs{CAPS}} (\acl{CAPS}) measured the properties of ion and electron populations in Saturn's plasma environment \citep{young_cassini_2004}
	\item \textbf{CDA} (\name{Cosmic Dust Analyzer}) investigated the properties of small ice and dust particles \citep{srama_cassini_2004}
	\item \textbf{INMS} (\name{Ion and Neutral Mass Spectrometer}) determined the composition and structure of ions and neutrals in the moons' environments and the magnetosphere of Saturn \citep{waite_cassini_2004}
	\item \textbf{MAG} (\name{Cassini Magnetometer}) measured the magnetic field properties of Saturn's environment \citep{dougherty_cassini_2004}
	\item \textbf{\acs{MIMI}} (\acl{MIMI}) measured the properties of high-energy ions and electrons and imaged energetic particles in Saturn's magnetosphere \citep{krimigis_magnetosphere_2004}
	\item \textbf{RPWS} (\name{Radio and Plasma Wave Spectrometer}) determined electrical and magnetic fields and perturbations in Saturn's plasma environment \citep{gurnett_cassini_2004}
\end{itemize}
Lastly, two radio wave instruments were part of the payload:
\begin{itemize}
	\item \textbf{Cassini Radar} mapped the surface of Titan \citep{elachi_radar:_2004}
	\item \textbf{RSS} (\name{Radio Science Subsystem}) conducted radio measurements of atmospheric and ionospheric properties as well as of the masses of objects in the Saturn system using \acs{NASA}'s \name{Deep Space Network} ground antennas \citep{kliore_cassini_2004}
\end{itemize}
Instruments of interest to the reader of this thesis are highlighted in red in Figure~\ref{fig::instr::cassini_sketch}. \ac{UVIS}, which the majority of the results presented here are based on, is described in more detail in the following section.

%=============================================================
\section{Cassini's Ultraviolet Imaging Spectrograph}
\label{sect::instr::uvis}

\begin{wrapfigure}{R}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/UVIS_assembly}
	\caption[Schematic of the \acs{UVIS} instrument]{Schematic of the \acs{UVIS} instrument, with the different sensors labelled. Image taken from \citet{esposito_cassini_2004}}
	\label{fig::instr::UVIS_assembly}
\end{wrapfigure}

% summary
\ac{UVIS} was a spectral imaging instrument in the \ac{UV} wavelength range and was comprised of two moderate-resolution telescope-spectrographs operating in the wavelength ranges 56 to 118\,nm (\acl{EUV}, or \acs{EUV}) and 110 to 190\,nm (\acl{FUV}, or \acs{FUV}), a high speed photometer, and a hydrogen deuterium absorption cell. Figure \ref{fig::instr::UVIS_assembly} shows the assembly of the different components.

% scientific goals
The \ac{UVIS} instrument had a broad range of scientific objectives. It proved a useful tool for determining the atmospheric and cloud properties and composition, and for analysing the atmospheric circulation and physics of Titan and Saturn. Using stellar occultation and spectroscopy, \ac{UVIS} contributed greatly to the investigation of the configuration and composition of Saturn's rings and their interrelation with Saturn's satellites. Furthermore, \ac{UVIS} data helped unravelling the characteristics of Saturn's icy moons.

% aurora stuff
Of a greater importance to this thesis however are the capabilities of \ac{UVIS} in observing Saturn's \ac{UV} auroral emissions, as the \ac{FUV} channel covers the wavelength range in which much of the \ac{UV} auroral emission falls. The signal in the \ac{FUV}'s spectral range is dominated by the H$_2$ Lyman and Werner bands and includes the H Lyman-$\alpha$ line; the \ac{FUV} sensor is therefore perfectly suited to capture the bulk of Saturn's auroral emissions.

% FUV construction and mechanism (single pixel/line)
The \ac{FUV} telescope-spectrograph, a schematic of which is shown in Figure \ref{fig::instr::UVIS_FUV}, has an aperture of $20\times 20\,\mathrm{mm^2}$. The incident light first reaches an off-axis parabolic mirror and is deflected through an interchangeable entrance slit of width $75/150/800\,\mathrm{\upmu{}m}$, providing \iac{FOV} of $0.75/1.5/8\times 64\,\mathrm{mrad^2}$, onto a toroidal grating. The grating has a 300\,mm horizontal radius of curvature and acts as a spectrometer. Both the mirror and the grating are coated with Al and MgF$_2$. Finally, the diffracted light falls onto a two-dimensional \ac{CODACON} detector. This detector plate counts and locates impinging photons with a resolution of 64 spatial pixels $\times$ 1024 spectral pixels. At one instance in time, the \ac{FUV} sensor can therefore image a line of 64 pixels at its full spectral resolution of 1024 frequency bins between 110 and 190\,nm. 

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{./img/instrumentation/UVIS_FUV}
	\caption[Schematic of the \acs{UVIS} \acs{FUV} sensor]{Schematic of the \acs{UVIS} \acs{FUV} sensor. Image taken from \citet{esposito_cassini_2004}}
	\label{fig::instr::UVIS_FUV}
\end{figure}

% calibrations
All intensities measured by the detector need to be corrected in several steps. While the sensitivity of the \ac{CODACON} has been measured in a laboratory environment before the launch of \name{Cassini}, it was observed to change as a function of wavelength and time such that continuous in-flight re-calibrations were necessary. This was achieved by observing the star Spica on a regular basis and adjusting calibrations following an exponential decay of sensitivity. After accounting for the detector sensitivity, the constant background caused by the \acp{RTG} has to be subtracted from the data. Lastly, anomalous \ac{CODACON} pixels have to be invalidated and the missing data interpolated.

% getting total unabsorbed H2 emission
Observations of auroral emissions on giant planets may also be considerably affected by hydrocarbon absorption, skewing the detectable emission. However, with some knowledge of the ionospheric composition it is possible to infer the total unabsorbed H$_2$ emission intensity in the \ac{UV} range ($70-170$\,nm) from the observed \ac{UVIS} spectra. For this reason, all \ac{UVIS} data used in this thesis is integrated between $155-162$\,nm and multiplied by a factor $8.1$ as described in \citet{gustin_characteristics_2016,gustin_statistical_2017}. A single \ac{UVIS} exposure is thereby reduced to a row of 64 pixel intensities of the total unabsorbed H$_2$ emission in the \ac{UV}, without any spectral information remaining.

\subsection{Combination of Auroral Images}
% scanning and constructing images
In order to obtain two-dimensional images, the spacecraft was slewed such that the \ac{UVIS} slit sweeped across the auroral region. Combining consecutive slit exposures yields a pseudo-image -- named such because different parts of the image have been recorded at different times and not over the same period such as for ``real'' images.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/proj_sample_2016_278T09_22_40.png}
	\caption[\acs{UVIS} polar projection procedure]{\ac{UVIS} polar projection procedure. (a) Calibrated and spectrally integrated dataset from 2016 \acs{DOY} 278, 09:22-11:56 \acs{UTC} (southern hemisphere), with \acs{UTC} time on the vertical axis and the 64 \ac{UVIS} pixels on the horizontal. All single slit exposures are simply arranged below one another, the integrated emission rate of each pixel shown in a logarithmic color scale (same scale as in panel b). Blue and red boxes frame the sections of the original dataset which were eventually projected or discarded, respectively. (b) Polar projection resulting from this dataset, with the same color scale. The view is from above the northern pole, observing the southern hemisphere through the planet. Local midnight is on the top, the Sun / local noon is at the bottom. Concentric grid rings mark the colatitude from the pole in steps of 10$^\circ$. (c) \name{Cassini}'s elevation angle above the horizon as seen from each grid bin.}
	\label{fig::instr::UVIS_projection}
\end{figure}

% projection routine
Each image is projected onto a $0.5^\circ\times0.25^\circ$ (lon\,$\times$\,lat) planetocentric polar grid at an altitude of $1100$\,km above Saturn’s $1$\,bar level ($R_\mathrm{E}=60,268$\,km, $R_\mathrm{P}=54,364$\,km) where the auroral emission profile was observed to maximize \citep{gerard_altitude_2009}. Figure \ref{fig::instr::UVIS_projection}a shows an example dataset from 2016\,\acs{DOY}\,278. The data has been calibrated and spectrally integrated as described above, such that only two dimensions remain (1151 slit exposures $\times$ 64 pixels). Shown here is the emission rate in kR observed by each pixel during each exposure (color scale shown in Fig. \ref{fig::instr::UVIS_projection}b), with the consecutive slit exposures arranged from the top downwards. Note that the two outermost pixels on both sides of the slit are unused -- they have shown anomalous signals and were therefore excluded in most auroral observations. In this example, \ac{UVIS} scanned over the auroral emission three times, imaging different parts of the auroral oval. After each scan, marked with a blue-dashed frame, \name{Cassini} quickly slewed back across the auroral oval to prepare for the following scan. These sections, marked with red-dashed frames, are excluded from the projection procedure due to the high attitude uncertainty and the lower spatial resolution during the quick attitude change. All data within the blue-dashed frames is then polar projected onto a planetocentric grid. This is done using \name{Cassini} SPICE pointing information available on the \ac{NASA} \name{Planetary Data System} to determine the exact viewing direction of each pixel (boresight and corners) and its intersection with the aurorae's ionospheric layer. The polar projection of the example dataset from Fig. \ref{fig::instr::UVIS_projection}a is shown in Figure \ref{fig::instr::UVIS_projection}b. Panel \ref{fig::instr::UVIS_projection}c shows \name{Cassini}'s elevation angle above the horizon at the location of each longitude-latitude grid bin.

It is important to note that this projection does not conserve photon counts, which means that auroral emission powers can only be obtained with knowledge of the spacecraft viewing geometry. A detailed analysis of this problem is given in appendix~\ref{chapt::app::radiance}. 

\subsection{Dayglow Removal}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/2016_277T06_36_29_sunlight_removal_comp}
	\caption[Removal of dayglow in UVIS images]{Demonstration of an algorithm removing dayglow from \ac{UVIS} images. (a) Polar-projected \ac{UVIS} image from 2016 \acs{DOY} 277, looking down onto the northern pole with midnight towards the top. Shown is the total unabsorbed H$_2$ emission intensity of Saturn’s northern aurorae with a logarithmic color scale as defined with the large colorbar to the right. Concentric rings mark the colatitude from the northern pole in steps of $10^\circ$. All data outside of the red-dashed line at $23^\circ$ colatitude is considered background emission and used for estimating the brightness of dayglow. (b) \ac{SZA} versus brightness histogram of all background pixels of all images within a $\pm3$\,h window around this observation, with the median overlaid in red. (c) Brightness map of dayglow derived from the median of the distribution in (b). (d) The original image with the derived background brightness (c) subtracted.}
	\label{fig::instr::UVIS_sunlight}
\end{figure}
Depending on the hemisphere and season at Saturn, some auroral images obtained by \ac{UVIS} may be contaminated with dayglow. The choice of \ac{UV} wavelength range used to derive the \ac{UV} brightness as described in the previous section mitigates this effect to some degree, but some contamination remains nevertheless. This dayglow is visible as a clear gradient in background brightness, increasing from midnight to noon. If the radiant flux, or ``auroral power'', is to be integrated from such images (see appendix \ref{chapt::app::radiance}) or absolute brightness values are to be compared, it may be necessary to remove this background in order to obtain reliable values.

Figure~\ref{fig::instr::UVIS_sunlight} demonstrates a simple algorithm which proved to reliably remove dayglow from \ac{UVIS} images. Using all \ac{UVIS} images of the same hemisphere obtained during a $\pm 3$\,h~window centered on the image which is to be corrected, we determine a distribution of \ac{SZA} versus background brightness. This is achieved by collecting all pixels situated at colatitudes $>23^\circ$ (red-dashed line in Figure~\ref{fig::instr::UVIS_sunlight}a) -- covering only regions well equatorward of the statistical equatorward boundary of auroral emissions \citep[e.g.,][]{badman_statistical_2006, carbary_morphology_2012, nichols_saturns_2016, kinrade_saturns_2018, bader_modulations_2019}. By median-filtering the \ac{SZA}-brightness distribution with a box $10^\circ$ wide in \ac{SZA}, we obtain a smooth, rather linear relation (see Fig.~\ref{fig::instr::UVIS_sunlight}b) which is used to model the background of dayglow for the entire region covered by the \ac{UVIS} image (Fig.~\ref{fig::instr::UVIS_sunlight}c). This modelled background brightness is subtracted from the original image, leaving only true auroral emissions (Fig.~\ref{fig::instr::UVIS_sunlight}d).

\section{The Cassini Magnetometer}
\label{sect::instr::mag}

The \name{Cassini} orbiter was equipped with two magnetometers: a fluxgate magnetometer and a vector helium magnetometer, both mounted on the magnetometer beam at different distances to allow for proper subtraction of the spacecraft's magnetic field contribution \citep{dougherty_cassini_2004}. However, the vector helium magnetometer failed soon after \name{Cassini}'s arrival at Saturn, leaving only the fluxgate magnetometer and necessitating more complex calibration and data cleaning procedures.

\begin{wrapfigure}{R}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/dougherty_2004_mag}
	\caption[Photo of \name{Cassini}'s fluxgate magnetometer]{Photo of \name{Cassini}'s fluxgate magnetometer and its electronics board. Taken from \citet{dougherty_cassini_2004}.}
	\label{fig::instr::MAG}
\end{wrapfigure}

The fluxgate magnetometer consisted of three single-axis ring core fluxgate sensors mounted orthogonally to each other, allowing the measurements of the magnetic field in three dimensions. A drive coil, wound around the high permeability ring core, generated a modulated magnetic field driving the core into saturation with alternating polarity at a frequency of $\sim 15$\,kHz. A second coil around the ring core then measured changes in the symmetry of the core's saturation as voltage changes which were analysed by the on-board electronics.

The magnetometer operated in four dynamic ranges, allowing magnetic field observations up to $\pm 44,000$\,nT at $5.4$\,nT resolution. Smaller dynamic ranges allowed for better resolutions, reaching down to $4.9$\,pT in the $\pm 40$\,nT range. The time resolution at which magnetic field vectors were downlinked to Earth was typically 32 measurements per second.


\section{Cassini's Magnetosphere Imaging Instrument}
\label{sect::instr::mimi}

\name{Cassini}'s \acf{MIMI} was an instrument package designed for performing in situ measurements of neutral and charged particles as well as global imaging of Saturn's magnetosphere \citep{krimigis_magnetosphere_2004}. Its three detector systems will be introduced below.

\subsection{Charge Energy Mass Spectrometer}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/krimigis_2004_chems}
	\caption[Schematic of the \acs{MIMI}-\acs{CHEMS} detector]{Schematic of the \acs{MIMI}-\acs{CHEMS} detector. Taken from \citet{krimigis_magnetosphere_2004}.}
	\label{fig::instr::CHEMS}
\end{figure}

The \acf{CHEMS} was designed to measure the three-dimensional distribution of suprathermal ion populations in Saturn's magnetosphere. It consisted of three telescopes placed around the spacecraft body, resulting in a nearly full $4\pi$ steradian \ac{FOV} when \name{Cassini} was spinning.

Each telescope was equipped with an electrostatic analyser followed by a time of flight detection system and a solid state detector. This allowed \ac{CHEMS} to determine an incoming particle's energy per charge, speed of motion and residual energy, respectively -- providing knowledge of its mass, charge state and energy. A sketch of one detector is shown in Figure~\ref{fig::instr::CHEMS}. \ac{CHEMS} was capable of measuring ions with energy per charge between $3-220$\,keV and ion species as heavy as Fe.

\subsection{Low-Energy Magnetospheric Measurement System}

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{./img/instrumentation/krimigis_2004_lemms}
	\caption[Schematic of the \acs{MIMI}-\acs{LEMMS} detector head]{Schematic of the \acs{MIMI}-\acs{LEMMS} detector head. Taken from \citet{krimigis_magnetosphere_2004}.}
	\label{fig::instr::LEMMS}
\end{figure}

The \ac{LEMMS} was a two-ended telescope used for measuring ions with energies $>30$\,keV and electrons with energies between 15\,keV and 1\,MeV. Each telescope end was equipped with solid state detectors, with one telescope end designed to cover the lower and the other covering the higher end of the measurable energy ranges. The entire telescope was heavily shielded against penetrating particles and mounted on a rotating platform to maximize its pitch angle coverage in combination with spacecraft rolling. However, the rotation mechanism unfortunately malfunctioned partway through \name{Cassini}'s Saturn tour, such that for the rest of the mission only specific viewing directions could be observed.

In the low energy end of the telescope, electrons were separated from ions and deflected by a static inhomogeneous magnetic field to hit different side-facing detectors while ions proceeded to detectors at the end of the telescope. The electron and ion signals were processed by a pulse height analyser to provide 64-channel energy spectra. The high energy end of the telescope was equipped with a stack of solid state detectors which measured the energy loss of energetic electrons and ions along their trajectory through the detector stack.

\subsection{Ion and Neutral Camera}

\ac{MIMI}'s \acf{INCA} was a time of flight sensor capable of detecting both ions and \acp{ENA} with energies from 7\,keV/nucleon to 3\,MeV/nucleon. It could provide information on the mass of each detected particle as well as its direction of motion and energy.

\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/krimigis_2004_inca}
	\caption[Schematic of MIMI-INCA]{Schematic of \acs{MIMI}-\acs{INCA}. Taken from \citet{krimigis_magnetosphere_2004}.}
	\label{fig::instr::INCA}
\end{wrapfigure}

With an instantaneous \ac{FOV} of $120^\circ \times 90^\circ$ and an angular resolution of up to $64\times 64$ pixels, it was able to either resolve a significant part of the local ion pitch angle distribution or to provide remote imagery of the global distribution of energetic ions using a technique known as \ac{ENA} imaging \citep[e.g.,][]{roelof_energetic_1987}. Switching between these two modes was done by changing the potential applied to the collimator plates (``Ion, e$^-$ sweeping plates'' in Figure~\ref{fig::instr::INCA}) to either allow charged particles to pass into the detector or let them instead be deflected into the collimator walls.

The time of flight detector was triggered when incoming particles penetrated the thin start foil -- secondary electrons were produced which entered the start \ac{MCP} to provide the start time. The original incident particle traveled further through the instrument until it encountered a second foil, located in front of the stop \ac{MCP}. Again, secondary electrons were produced and entered the \ac{MCP}, registering the stop time. The mass of an incident particle was determined by the pulse height of the \ac{MCP} signal, as the number of secondary electrons produced increases with the particle mass. The pulse height was sufficient to distinguish between the two most common neutrals in Saturn's magnetosphere, hydrogen and oxygen, but further mass discrimination was not feasible.

\section{The Cassini Radio and Plasma Wave Spectrometer}
\label{sect::instr::rpws}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/gurnett_2004_rpws}
	\caption[Sketch of radio and plasma wave features in Saturn's magnetosphere]{Sketch of radio and plasma wave features observable in Saturn's magnetosphere. Taken from \citet{gurnett_cassini_2004}.}
	\label{fig::instr::RPWS}
\end{figure}

The \acf{RPWS} was used to study radio emissions and plasma waves in Saturn's magnetosphere \citep{gurnett_cassini_2004}. A sketch of the different radio and plasma wave features known to occur is shown in Figure~\ref{fig::instr::RPWS}.

It was equipped with three nearly orthogonal electric field antennas measuring electric fields at frequencies between 1\,Hz and 16\,MHz, three orthogonal search coil magnetic antennas measuring magnetic fields at frequencies between 1\,Hz and 12\,kHz as well as a Langmuir probe used for determining the electron density and temperature.

A number of different receiver systems was used to analyse the antenna signals. The high frequency, medium frequency and low frequency receiver together covered a frequency range from 1\,Hz to 16\,MHz, while a five-channel waveform receiver could process observations from up to five antennas simultaneously in different spectral ranges. A wideband receiver completed the set of receivers, making \ac{RPWS} the most advanced radio spectrometer to investigate Saturn's magnetosphere to date.

 
%===========================
\section{The Hubble Space Telescope}
\label{sect::instr::hst}
The \acf{HST} is a 2.4\,m reflecting telescope observing in the near-\acs{UV}, visible, and near-\acs{IR} bands, built by the \ac{NASA} and operated in a coordinated programme together with the \ac{ESA}. It was placed into orbit in 1990 and remains in operation, providing scientific data of immense importance to the astronomical community. It is positioned in a geocentric low Earth orbit at an altitude of about 540\,km, allowing observations unimpeded by atmospheric effects.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/hst_lallo_2012}
	\caption[Schematic of the \acs{HST}]{Schematic highlighting the \ac{HST}'s main components. The imaging instruments used for auroral observation campaigns are indicated as ``Axial Science Instruments'' at the rear of the telescope. Image taken from \citep{lallo_experience_2012}.}
	\label{fig::instr::HST_schematic}
\end{figure}

A basic schematic of the \ac{HST} is shown in Figure \ref{fig::instr::HST_schematic}. The optical assembly constitutes the biggest part of the spacecraft, with the scientific instruments located towards the rear. Currently there are four instruments in operation:
\begin{itemize}
	\item \textbf{ACS} (\name{Advanced Camera for Surveys}) is a highly versatile imaging instrument operating between the near-\acs{UV} and near-{IR} bands
	\item \textbf{COS} (\name{Cosmic Origins Spectrograph}) performs \ac{UV} spectroscopy of faint point sources
	\item \textbf{\acs{STIS}} (\acl{STIS}) is mainly used for imaging and spectrography in the \ac{UV} spectrum and is the main instrument used for observing aurorae on the outer planets
	\item \textbf{WFC3} (\name{Wide Field Camera 3}) is designed for imaging in the visible spectrum
\end{itemize}

\noindent Most auroral observations from Saturn used in this thesis were taken by the \acf{STIS}, described in detail in \citet{woodgate_space_1998}. It was installed on \ac{HST} in 1997 and is still in operation. Its optics are designed to correct for the spherical aberration of \ac{HST}'s primary mirror before the beam of light entering the instrument is directed according to the observation mode and detector required. After passing either gratings for the spectroscopy modes or apertures for the imaging modes, the beam eventually hits one of \ac{STIS}' three detectors: two \ac{MAMA} detectors for the \ac{UV} and a \ac{CCD} for the visible bands. Auroral emissions at Saturn maximize in the \ac{FUV} wavelength range, which is covered by one of the \ac{MAMA} detectors designed for observations within $115-170$\,nm. Incident photons produce electrons as they hit the CsI photocathode, which are accelerated into \iac{MCP} where the charge is detected by an anode array. The \ac{MAMA} detector is hence able to record the time and position on the detector of each incident photon (time tag mode) or simply integrate the signal to an image over a fixed exposure time. For auroral observations, a SrF$_2$ filter (bandpass $125-190$\,nm) is often used to block the H Lyman-$\alpha$ line (121\,nm) in order to remove potential geocoronal contamination.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/proj_sample_2014_100T02_29_09.png}
	\caption[\acs{HST} polar projection procedure]{\ac{HST} polar projection procedure. (a) \acs{HST} \acs{STIS} image from 2014 \acs{DOY} 100, 02:29 Saturn \acs{UTC} (northern hemisphere). The original photon count rate per pixel, smoothed with a 11\,$\times$\,11 pixel$^2$ box, is shown in an arbitrary color scale. (b) Detail of the same image, highlighting the auroral region. (c) Background-subtracted polar projection resulting from this image, the emission rate color-coded. The view is from above the northern pole, with local midnight on the top and the Sun / local noon at the bottom. Concentric grid rings mark the colatitude from the pole in steps of 10$^\circ$. (d) \acs{HST}'s elevation angle above the horizon as seen from each grid bin.}
	\label{fig::instr::HST_projection}
\end{figure}

All \ac{HST} images were background subtracted, corrected for geometric distortion and projected onto a planetocentric polar grid \citep{clarke_response_2009}. A comparison between a raw \ac{HST} image and its projection is shown in Figure \ref{fig::instr::HST_projection}.



%===========================
\section{Planetary Period Oscillation Longitude Systems}
\label{sect::instr::ppo}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/ppo_model.pdf}
	\caption[PPO perturbation field and phase angles]{Sketch showing the northern \ac{PPO} system as seen from above the northern hemisphere. (a) Saturn's equatorial magnetosphere, with noon towards the bottom and dusk towards the right. The magnetopause is indicated with a grey dash-dotted line, following the model of \citet{arridge_modeling_2006} with $p_\mathrm{dyn}=0.05$\,nPa. The inner grey dashed circle marks the approximate location where the main \ac{PPO}-related \acp{FAC} pass through the equatorial plane at $\sim 15R_\mathrm{S}$. At the instant shown, the northern \ac{PPO} phase is $\Phi_\mathrm{N} (t) = 30^\circ$, a black arrow marking the orientation of the associated equatorial model perturbation dipole. The magnetic perturbation field lines of the northern \ac{PPO} system are shown as solid grey lines, following, e.g., \citet{provan_phase_2009} and \citet{andrews_magnetic_2010}. The principal meridians of the \ac{PPO} phase function are shown by the black arrow and its perpendicular, the phase values $\Psi_\mathrm{N}$ increasing clockwise as indicated. The direction of \acp{FAC} passing the equatorial plane due to the perturbation field is marked in red, with circled crosses indicating a flow into and circled dots a flow out of the plane of the figure. (b) The corresponding view of Saturn's northern ionosphere as seen from above the north pole, again with noon towards the bottom. Bold numbers around the edge of the panel indicate the \ac{LT}, dashed circles mark the northern colatitude from the pole in $10^\circ$ steps. The orientation of the model perturbation dipole and the \ac{PPO} principal meridians are marked in black as in panel (a). Ionospheric upward (downward) \ac{FAC} regions at auroral latitudes are indicated with red circled dots (crosses). Blue lines and arrows sketch the driving neutral atmospheric and ionospheric flows in the northern hemisphere \citep{jia_driving_2012, jia_driving_2012-1, hunt_field-aligned_2014, hunt_field-aligned_2015}.}
	\label{fig::instr::ppo_sketch}
\end{figure}

This section describes longitude systems which are based on \ac{PPO} magnetic field periodicities. The momentary orientation of each \ac{PPO} system is hereby defined by the \ac{PPO} dipole phase angle $\Phi_\mathrm{N,S} (t)$, which describes the azimuthal angle at time $t$ at which the northern/southern \ac{PPO}-related equatorial magnetic perturbation fields point radially outward from the planet. $\Phi_\mathrm{N,S} (t)$ is hereby measured from noon and in the direction of Saturn's rotation. Figure~\ref{fig::instr::ppo_sketch} shows the northern \ac{PPO} system at $\Phi_\mathrm{N} (t) = 30^\circ$, a black arrow indicating the \ac{PPO} perturbation dipole in the equatorial plane.

The \ac{PPO} phases $\Psi_\mathrm{N,S} (\varphi, t)$ then provide longitude systems rotating with the \ac{PPO} dipoles. They are defined as
\begin{equation}
	\Psi_\mathrm{N,S} (\varphi, t) = \Phi_\mathrm{N,S} - \varphi
\end{equation}
with $\varphi$ being the azimuthal angle of any spatial point measured from local noon. In Fig.~\ref{fig::instr::ppo_sketch}, four $\Psi_\mathrm N$ longitudes are marked in black.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{./img/instrumentation/general_model_h.pdf}
	\caption[Ionospheric PPO FAC regions]{\ac{PPO}-related rotating \ac{FAC} patterns in Saturn's polar ionospheres, shown in \ac{PPO} longitude systems. (Left) \ac{PPO}-associated \acp{FAC} in the northern polar ionosphere. Red-circled dots (crosses) mark \acp{FAC} flowing out of (into) the figure plane, corresponding to upward (downward) currents relative to Saturn's surface. The section highlighted in red marks where an intensification of the aurorae is expected. The atmospheric/ionospheric flow pattern in northern hemisphere, driving the northern \ac{PPO} perturbations, is drawn in blue. (right) Corresponding sketch for the southern hemisphere, looking through the planet from the north. Circled dots (crosses) mark downward (upward) currents now, and blue lines and arrows mark the atmospheric/ionospheric flows in the southern hemisphere which drive the southern \ac{PPO} system.}
	\label{fig::instr::ppo_iono_sketch}
\end{figure}

\ac{PPO}-associated \acp{FAC} are always located at the same \ac{PPO} phase. The \acp{FAC} related to the northern \ac{PPO} system are indicated in red in Fig.~\ref{fig::instr::ppo_sketch}, with \ref{fig::instr::ppo_sketch}a showing the currents (partly) passing through the equatorial plane from the northern to the southern hemisphere (circled red crosses) and in the opposite direction (circled red dots) - roughly half of the \ac{PPO}-associated currents diverge from the field-aligned direction and close in the equatorial plane \citep{bradley_field-aligned_2018}. Figure~\ref{fig::instr::ppo_sketch}b shows the footpoints of these currents in Saturn's northern polar ionosphere, upward (downward) currents being indicated with red circled dots (crosses). The atmospheric/ionospheric vortical flow pattern thought to generate the northern \ac{PPO} perturbations are shown in blue. The southern \ac{PPO} system, generated by oppositely directed atmospheric/ionospheric flows in the southern hemisphere, effects the same pattern of \acp{FAC} in the northern hemisphere in its own longitude system (see Figure~\ref{fig::instr::ppo_iono_sketch}).

The \ac{PPO} dipole phase angles $\Phi_\mathrm{N,S} (t)$ used in this study were derived from sinusoidal fits to \name{Cassini} magnetic field data as thoroughly described in, e.g., \citep{andrews_planetary_2012, provan_planetary_2013, provan_planetary_2014, provan_planetary_2015, provan_planetary_2016, provan_planetary_2018}.