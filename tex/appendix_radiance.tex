\section{The Rayleigh and Radiant Flux}
The brightness unit used in auroral physics is the \textbf{Rayleigh}, named after the fourth \name{Lord Rayleigh}, \name{R.J.~Strutt}, 1875-1947, who made first measurements of night airglow \citep{rayleigh_absolute_1930}. It describes the \textbf{column emission rate} $I$, assuming an isotropic source with no self absorption and a column of unspecified length \citep{hunten_photometric_1956}, and is defined as
\begin{equation}
	1\,\mathrm{R} = 10^{10}\,\mathrm{\frac{Photons}{s\,m^2\,column}}
\end{equation}
in SI units \citep{baker_rayleigh:_1976}. We can convert the \textbf{column emission rate} $I$ into the \textbf{radiance} $L$ by using \citep{baker_rayleigh:_1976}
\begin{equation}
	L\left[\mathrm{\frac{Photons}{s\,m^2\,sr}}\right] = \frac{10^{10}}{4\pi} \cdot I\,[\mathrm{R}],
\end{equation}
and for the in auroral physics usually preferred \textit{Kilorayleigh}, kR, obviously
\begin{equation}
	L\left[\mathrm{\frac{Photons}{s\,m^2\,sr}}\right] = \frac{10^{13}}{4\pi} \cdot I\,[\mathrm{kR}].
\end{equation}
The \textbf{radiant flux} $\Phi$ is obtained by integrating over the full $4\pi\,\mathrm{sr}$ solid angle, and the apparent emitting surface area $A$ seen from the detector in $\mathrm{m^2}$,
\begin{equation}
	\Phi\,\left[\mathrm{\frac{Photons}{s}}\right] = L\left[\mathrm{\frac{Photons}{s\,m^2\,sr}}\right] \cdot 4\pi\,\mathrm{sr} \cdot A\,\left[\mathrm{m^2}\right].
\end{equation}
Note that the radiant flux is only meaningful in combination with spectral information, i.e. the mean energy of the emitted photons. Therefore, multiplying by the mean H$_2$ photon energy of $1.6\cdot 10^{-18}\,\mathrm{\frac{J}{Photon}}$ \citep{kurth_saturn_2016} gives the radiant flux, often also named ``auroral power'', in SI units,
\begin{equation}
	\Phi\,\left[\mathrm{W}\right] = 1.6\cdot 10^{-18}\,\mathrm{\frac{J}{Photon}} \cdot\Phi\,\left[\mathrm{\frac{Photons}{s}}\right].
\end{equation}

\section{Emitting Surface Area}
\subsection{Unprojected \acs{HST} Images}
The emission area for \iac{HST} pixel is equal to the perpendicular area this pixel covers at the distance of Saturn. This area is
\begin{equation}
	A\,[\mathrm{m^2}] = r^2\cdot \sin(\alpha_1)\cdot\sin(\alpha_2)
\end{equation}
with $r$ as the distance between \ac{HST} and Saturn and $\alpha_{1/2}$ as the angular \ac{FOV} of a pixel. For the \ac{STIS} sensor with a \ac{FOV} of $25\times 25$\,arcsec$^2$ and 1024 pixels across in each direction, we find
\begin{equation}
	\alpha_{1/2} = 25/1024\,\mathrm{arcsec}
\end{equation}
such that
\begin{equation}
	A\,[\mathrm{m^2}] = \left(r\,[\mathrm{m}]\cdot \sin(25/1024\,\mathrm{arcsec})\right)^2
\end{equation}
or
\begin{equation}
	A\,[\mathrm{m^2}] = \left(r\,[\mathrm{km}]\cdot 10^{3} \cdot \sin(25/1024\,\mathrm{arcsec})\right)^2.
\end{equation}

\subsection{Projected \acs{HST} and \acs{UVIS} Images}

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{./img/appendix/projection_wireframe_01}
	\caption[Example \acs{FOV} of an instrument pixel on the planetary surface]{Example \ac{FOV} of an observational instrument pixel projected on the surface of the planet, not to scale. The longitude-latitude grid drawn on the planet is also shown in the instrument plane.}
	\label{fig::append::projection}
\end{figure}

The radiant flux can also be determined from an image which has already been projected onto a longitude-latitude grid. However, care has to be taken when the radiance $L$ is integrated over the emitting surface.

As shown in Figure \ref{fig::append::projection}, instrument pixel footprints on the planetary surface are skewed depending on the angle under which the emission ``surface'', the ionospheric layer in which auroras are generated, is being observed. The absolute surface area of one longitude-latitude grid bin is therefore not equal to the corresponding apparent emitting surface seen from the spacecraft based on which the column emission rate has been determined. With small grid bin sizes, each grid bin can be approximated by a flat plane such that its apparent emitting surface $A_\mathrm{app}$ and absolute surface $A_\mathrm{abs}$ are related through
\begin{equation}
	A_\mathrm{app} = \sin{\varepsilon}\cdot A_\mathrm{abs}
\end{equation}
with $\varepsilon$ as the elevation angle of the observing instrument above the horizon as seen from the grid bin location.
