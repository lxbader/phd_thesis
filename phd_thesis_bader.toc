\select@language {english}
\contentsline {chapter}{Abstract}{iii}{chapter*.1}
\contentsline {chapter}{Declaration}{v}{chapter*.2}
\contentsline {chapter}{List of Publications}{vii}{chapter*.3}
\contentsline {chapter}{Acknowledgements}{xi}{chapter*.6}
\contentsline {chapter}{Contents}{xv}{section*.7}
\contentsline {chapter}{\nonumberline List of Figures}{xvii}{chapter*.9}
\contentsline {chapter}{\nonumberline List of Tables}{xix}{chapter*.10}
\contentsline {chapter}{List of Acronyms}{xxi}{chapter*.11}
\contentsline {chapter}{Preface}{1}{chapter*.12}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Space Plasmas}{3}{section.1.1}
\contentsline {subsection}{\nonumberline The Plasma State}{3}{section*.13}
\contentsline {subsection}{\nonumberline Single Particle Motion}{4}{section*.14}
\contentsline {subsection}{\nonumberline Kinetic Theory and Phase Space}{8}{section*.17}
\contentsline {subsection}{\nonumberline Magnetohydrodynamics}{9}{section*.18}
\contentsline {subsection}{\nonumberline Field-aligned Currents}{12}{section*.20}
\contentsline {subsection}{\nonumberline Plasma Waves and Wave-Particle Interactions}{12}{section*.21}
\contentsline {section}{\numberline {1.2}The Heliosphere}{14}{section.1.2}
\contentsline {chapter}{\numberline {2}Saturn's Magnetosphere}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}The Magnetospheric Structure of Saturn}{19}{section.2.1}
\contentsline {subsection}{\nonumberline The Internal Magnetic Field}{20}{section*.26}
\contentsline {subsection}{\nonumberline Enceladus: The Main Source of Magnetospheric Plasma}{21}{section*.27}
\contentsline {subsection}{\nonumberline Radiation Belts}{23}{section*.30}
\contentsline {subsection}{\nonumberline Ring Current}{25}{section*.32}
\contentsline {subsection}{\nonumberline Magnetodisc and Magnetotail}{25}{section*.33}
\contentsline {section}{\numberline {2.2}The Dynamics of Saturn's Magnetosphere}{27}{section.2.2}
\contentsline {subsection}{\nonumberline Global Transport of Mass and Energy}{27}{section*.35}
\contentsline {subsection}{\nonumberline Corotation Breakdown and Magnetosphere-Ionosphere Coupling}{30}{section*.37}
\contentsline {subsection}{\nonumberline Rotational Modulation}{32}{section*.40}
\contentsline {section}{\numberline {2.3}Saturn's Ultraviolet Aurorae - an Overview}{35}{section.2.3}
\contentsline {subsection}{\nonumberline Auroral Acceleration}{35}{section*.44}
\contentsline {subsection}{\nonumberline Generation of Auroral Emissions}{37}{section*.46}
\contentsline {subsection}{\nonumberline Auroral Signatures and Morphology}{38}{section*.48}
\contentsline {paragraph}{\nonumberline Main Emission:}{38}{section*.50}
\contentsline {paragraph}{\nonumberline Outer Emission:}{41}{section*.51}
\contentsline {paragraph}{\nonumberline Enceladus Footprint:}{41}{section*.52}
\contentsline {paragraph}{\nonumberline Dayside Transients:}{42}{section*.53}
\contentsline {paragraph}{\nonumberline Dawn brightenings:}{42}{section*.55}
\contentsline {paragraph}{\nonumberline Transient auroral flashes:}{44}{section*.57}
\contentsline {chapter}{\numberline {3}Instrumentation and Methods}{45}{chapter.3}
\contentsline {section}{\numberline {3.1}The Cassini-Huygens Mission}{45}{section.3.1}
\contentsline {section}{\numberline {3.2}Cassini's Ultraviolet Imaging Spectrograph}{48}{section.3.2}
\contentsline {subsection}{\nonumberline Combination of Auroral Images}{50}{section*.61}
\contentsline {subsection}{\nonumberline Dayglow Removal}{52}{section*.63}
\contentsline {section}{\numberline {3.3}The Cassini Magnetometer}{54}{section.3.3}
\contentsline {section}{\numberline {3.4}Cassini's Magnetosphere Imaging Instrument}{55}{section.3.4}
\contentsline {subsection}{\nonumberline Charge Energy Mass Spectrometer}{55}{section*.66}
\contentsline {subsection}{\nonumberline Low-Energy Magnetospheric Measurement System}{56}{section*.68}
\contentsline {subsection}{\nonumberline Ion and Neutral Camera}{57}{section*.70}
\contentsline {section}{\numberline {3.5}The Cassini Radio and Plasma Wave Spectrometer}{58}{section.3.5}
\contentsline {section}{\numberline {3.6}The Hubble Space Telescope}{59}{section.3.6}
\contentsline {section}{\numberline {3.7}Planetary Period Oscillation Longitude Systems}{63}{section.3.7}
\contentsline {chapter}{\numberline {4}Publication I -- \textit {Statistical planetary period oscillation signatures in Saturn's UV auroral intensity}}{65}{chapter.4}
\contentsline {paragraph}{\nonumberline Author contribution statement}{65}{section*.77}
\contentsline {chapter}{\numberline {5}Publication II -- \textit {Modulations of Saturn's UV auroral oval location by planetary period oscillations}}{81}{chapter.5}
\contentsline {paragraph}{\nonumberline Author contribution statement}{81}{section*.79}
\contentsline {chapter}{\numberline {6}Publication III -- \textit {Observations of continuous quasiperiodic auroral pulsations on Saturn in high time-resolution UV auroral imagery}}{101}{chapter.6}
\contentsline {paragraph}{\nonumberline Author contribution statement}{101}{section*.81}
\contentsline {chapter}{\numberline {7}Publication IV -- \textit {The dynamics of Saturn's main aurorae}}{117}{chapter.7}
\contentsline {paragraph}{\nonumberline Author contribution statement}{117}{section*.83}
\contentsline {chapter}{\numberline {8}Publication V -- \textit {Energetic particle signatures above Saturn's aurorae}}{131}{chapter.8}
\contentsline {paragraph}{\nonumberline Author contribution statement}{131}{section*.85}
\contentsline {chapter}{\numberline {9}Publication VI -- \textit {The morphology of Saturn's aurorae observed during the Cassini Grand Finale}}{149}{chapter.9}
\contentsline {paragraph}{\nonumberline Author contribution statement}{149}{section*.87}
\contentsline {chapter}{\numberline {10}Discussion and Conclusions}{161}{chapter.10}
\contentsline {section}{\numberline {10.1}The Different Drivers of Saturn's Main Aurorae}{161}{section.10.1}
\contentsline {section}{\numberline {10.2}Planetary Period Oscillations as a Near Omnipresent Modulation}{162}{section.10.2}
\contentsline {section}{\numberline {10.3}Transient Small-Scale Structures}{163}{section.10.3}
\contentsline {section}{\numberline {10.4}On Auroral Acceleration at Saturn}{165}{section.10.4}
\contentsline {chapter}{\leavevmode {\color {Maroon}Appendices}}{}{section*.89}
\contentsline {chapter}{\numberline {A}Radiance Conversions}{167}{appendix.A}
\contentsline {section}{\numberline {A.1}The Rayleigh and Radiant Flux}{167}{section.A.1}
\contentsline {section}{\numberline {A.2}Emitting Surface Area}{168}{section.A.2}
\contentsline {subsection}{\nonumberline Unprojected \acs {HST} Images}{168}{section*.90}
\contentsline {subsection}{\nonumberline Projected \acs {HST} and \acs {UVIS} Images}{168}{section*.91}
\contentsline {chapter}{\numberline {B}Supplementary Information}{171}{appendix.B}
\contentsline {section}{\numberline {B.0}Supplementary Information for Publication II}{173}{section*.93}
\contentsline {section}{\numberline {B.0}Supplementary Information for Publication III}{183}{section*.94}
\contentsline {section}{\numberline {B.0}Supplementary Information for Publication IV}{225}{section*.95}
\contentsline {section}{\numberline {B.0}Supplementary Information for Publication V}{233}{section*.96}
\contentsline {chapter}{References}{237}{chapter*.97}
